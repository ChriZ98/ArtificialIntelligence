#pragma once
#include "core/Core.h"
#include "core/Input.h"
#include "rendering/Window.h"
#include "rendering/Font.h"
#include "rendering/MovingCam.h"

using namespace zindach_game_engine;

constexpr size_t row_size = 250;
constexpr float offset = -1.0f + 1.0f / row_size;
constexpr float factor = 2.0f / row_size;

class ConwaysGame : public Core {
	unsigned int alive_cells_;
	Window window_;
	Program program_;
	std::vector<bool> grid_;
	std::vector<vec2> offsets_;

public:
	ConwaysGame();
	void update(float frame_time) override;
	void render() override;
};
