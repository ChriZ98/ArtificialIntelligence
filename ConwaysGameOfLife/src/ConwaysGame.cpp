#include "ConwaysGame.h"
#include <vector>
#include "rendering/Font.h"
#include "rendering/Program.h"
#include "rendering/Shader.h"
#include <queue>

using namespace zindach_game_engine;

ConwaysGame::ConwaysGame() : Core(30), alive_cells_(0), window_("MyGame", 900, 900, vec2(1900 - 900, 1020 - 900)),
                             grid_(row_size * row_size, false) {
	const int shape = 0;

	switch (shape) {
	case 0:
		for (size_t i = 0; i < row_size * row_size; i += 3) {
			grid_[i] = true;
		}
		break;
	case 1:
		for (size_t i = 0; i < row_size; i += 1) {
			grid_[i * row_size + i] = true;
		}
		for (size_t i = 0; i < row_size; i += 1) {
			grid_[i * row_size + row_size - i - 1] = true;
		}
		break;
	case 2:
		for (size_t i = 0; i < row_size; i += 1) {
			grid_[row_size * row_size / 2 + i] = true;
		}
		for (size_t i = 0; i < row_size; i += 1) {
			grid_[i * row_size + row_size / 2] = true;
		}
		break;
	case 3:
		for (size_t i = 0; i < row_size; i += 1) {
			for (size_t j = i; j < row_size; j += 2) {
				grid_[i * row_size + j] = true;
			}
		}
		break;
	case 4:
		for (size_t i = 250; i < 750; i += 1) {
			for (size_t j = 250; j < 750; j += 1) {
				if ((i < 450 || i > 550) && (j < 450 || j > 550))
					grid_[i * row_size + j] = true;
			}
		}
		break;
	case 5:
		for (size_t i = 0; i < row_size; i += 1) {
			for (size_t j = 0; j < row_size; j += 1) {
				if ((i < 450 || i > 550) && (j < 450 || j > 550))
					grid_[i * row_size + j] = true;
			}
		}
		break;
	default: break;
	}

	const Shader vert_shader(VERTEX_SHADER, "res/shader/ShapeVert.glsl");
	const Shader frag_shader(FRAGMENT_SHADER, "res/shader/ShapeFrag.glsl");
	program_ = Program({vert_shader.shader_id, frag_shader.shader_id});

	const float size = 1.0f / row_size;

	unsigned int vertex_buffer, index_buffer;
	glGenBuffers(1, &vertex_buffer);
	glGenBuffers(1, &index_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float),
	             std::vector<float>{-size, -size, 0.0f, size, -size, 0.0f, size, size, 0.0f, -size, size, 0.0f}.data(),
	             GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), std::vector<unsigned int>{0,1,2,0,2,3}.data(),
	             GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	program_.use();
}

void ConwaysGame::update(const float frame_time) {
	alive_cells_ = 0;
	offsets_.clear();
	std::vector<bool> old(grid_);
	for (size_t i = 0; i < row_size; ++i) {
		for (size_t j = 0; j < row_size; ++j) {
			unsigned int alive_count = 0;
			for (size_t k = i == 0 ? 0 : i - 1; k < (i == row_size - 1 ? row_size : i + 2); ++k) {
				for (size_t l = j == 0 ? 0 : j - 1; l < (j == row_size - 1 ? row_size : j + 2); ++l) {
					if (old[k * row_size + l]) {
						++alive_count;
					}
				}
			}
			if (old[i * row_size + j]) {
				if (alive_count - 1 < 2 || alive_count - 1 > 3) {
					grid_[i * row_size + j] = false;
				} else {
					++alive_cells_;
					offsets_.emplace_back(j * factor + offset, i * factor + offset);
				}
			} else {
				if (alive_count == 3) {
					grid_[i * row_size + j] = true;
					++alive_cells_;
					offsets_.emplace_back(j * factor + offset, i * factor + offset);
				}
			}
		}
	}
}

void ConwaysGame::render() {
	window_.clear();

	const unsigned int offsets_location = glGetUniformLocation(program_.program_id, "offsets");

	while (alive_cells_ > 1024) {
		glUniform2fv(offsets_location, 1024, reinterpret_cast<float*>(offsets_.data()));

		glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr, 1024);
		alive_cells_ -= 1024;
		offsets_.erase(offsets_.begin(), offsets_.begin() + 1024);
	}
	if (alive_cells_ > 0) {
		glUniform2fv(offsets_location, alive_cells_, reinterpret_cast<float*>(offsets_.data()));

		glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr, alive_cells_);
	}

	window_.update();
}
