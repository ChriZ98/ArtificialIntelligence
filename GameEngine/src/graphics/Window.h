#pragma once
#include "stdafx.h"

using namespace zindach_math;

namespace zindach_game_engine {

class Window {
    bool resized_;

    static void error_callback(const int error, const char *description);
    static void on_window_resized(GLFWwindow *window, const int width, const int height);
    static void on_window_closed(GLFWwindow *window);
    static void debug(const std::string &title, const vec2_ui &dimensions, const bool borderless, const vec2_ui &pos,
                      const bool resizable);

public:
    bool resizable;
    bool should_close;
    std::string title;
    vec2_ui dimensions;
    vec2_f center;
    GLFWwindow *glfw_window;

    Window(const std::string &title, const bool fullscreen, const vec2_ui &dimensions = vec2_ui(),
           const bool borderless = true, const vec2_ui &pos = vec2_ui(), const bool resizable = false);

    void show() const;
    void hide() const;
    bool resized();

    static void update();
    void destroy() const;
};
}
