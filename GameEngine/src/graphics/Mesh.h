﻿#pragma once
#include "stdafx.h"
#include "Vertex.h"

namespace zindach_game_engine {

class Mesh {
public:
    std::vector<Vertex> vertices;

    explicit Mesh(const std::vector<Vertex> &vertices);
};
}
