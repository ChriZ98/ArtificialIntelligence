﻿#include "stdafx.h"
#include "UniformBufferObject.h"

namespace zindach_game_engine {
UniformBufferObject::UniformBufferObject(const zindach_math::mat4_f &model, const zindach_math::mat4_f &view,
                                         const zindach_math::mat4_f &projection)
    : model(model),
      view(view),
      projection(projection) {}

void *UniformBufferObject::get_mvp() {
    mvp = projection * view * model;
    mvp = transpose(mvp);
    return &mvp;
}
}
