﻿#include "stdafx.h"
#include "Vertex.h"

namespace zindach_game_engine {

Vertex::Vertex(const zindach_math::vec2_f &pos, const zindach_math::vec3_f &color)
    : pos(pos), color(color) {}

vk::VertexInputBindingDescription Vertex::get_binding_description() {
    vk::VertexInputBindingDescription binding_description;
    binding_description.binding = 0;
    binding_description.stride = sizeof(Vertex);

    return binding_description;
}

std::array<vk::VertexInputAttributeDescription, 2> Vertex::get_attribute_descriptions() {
    std::array<vk::VertexInputAttributeDescription, 2> descriptions;

    descriptions[0].binding = 0;
    descriptions[0].location = 0;
    descriptions[0].format = vk::Format::eR32G32Sfloat;
    descriptions[0].offset = offsetof(Vertex, pos);

    descriptions[1].binding = 0;
    descriptions[1].location = 1;
    descriptions[1].format = vk::Format::eR32G32B32Sfloat;
    descriptions[1].offset = offsetof(Vertex, color);

    return descriptions;
}
}
