#include "stdafx.h"
#include "Window.h"
#include "core/DebugUtils.h"
#include "core/Logger.h"

namespace zindach_game_engine {

Window::Window(const std::string &title, const bool fullscreen, const vec2_ui &dimensions, const bool borderless,
               const vec2_ui &pos, const bool resizable)
    : resizable(resizable), should_close(false), title(title), dimensions(dimensions),
      center(static_cast<vec2_f>(dimensions) / 2.0f) {
    LOG(INFO, "Init GLFW");
    if (!glfwInit()) {
        LOG(ERROR, "Failed to initialize GLFW");
    }

    glfwSetErrorCallback(error_callback);

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

    LOG(INFO, "Init window");
    debug(title, dimensions, borderless, pos, resizable);

    if (fullscreen) {
        if (borderless) {
            const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            glfwWindowHint(GLFW_RED_BITS, mode->redBits);
            glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
            glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
            glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

            glfw_window = glfwCreateWindow(mode->width, mode->height, title.c_str(), glfwGetPrimaryMonitor(), nullptr);
            this->dimensions = vec2_ui(static_cast<unsigned>(mode->width), static_cast<unsigned>(mode->height));
        } else {
            glfw_window = glfwCreateWindow(dimensions(0), dimensions(1), title.c_str(), glfwGetPrimaryMonitor(), nullptr);
        }
    } else {
        if (!resizable) {
            glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        }
        if (borderless) {
            glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
        }
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

        glfw_window = glfwCreateWindow(dimensions(0), dimensions(1), title.c_str(), nullptr, nullptr);
    }

    if (!glfw_window) {
        glfwTerminate();
        LOG(ERROR, "Failed to initialize window");
    }

    glfwSetWindowUserPointer(glfw_window, this);
    glfwSetWindowSizeCallback(glfw_window, on_window_resized);
    glfwSetWindowCloseCallback(glfw_window, on_window_closed);

    if (!fullscreen) {
        glfwSetWindowPos(glfw_window, pos(0), pos(1));
    }
}

void Window::show() const {
    glfwShowWindow(glfw_window);
}

void Window::hide() const {
    glfwHideWindow(glfw_window);
}

bool Window::resized() {
    if (!resized_) {
        return false;
    }

    resized_ = false;
    return true;
}

void Window::update() {
    glfwPollEvents();
}

void Window::destroy() const {
    glfwDestroyWindow(glfw_window);
    glfwTerminate();
}

void Window::error_callback(const int error, const char *description) {
    std::string error_name;

    switch (error) {
        case GLFW_NOT_INITIALIZED:
            error_name = "GLFW_NOT_INITIALIZED";
            break;
        case GLFW_NO_CURRENT_CONTEXT:
            error_name = "GLFW_NO_CURRENT_CONTEXT";
            break;
        case GLFW_INVALID_ENUM:
            error_name = "GLFW_INVALID_ENUM";
            break;
        case GLFW_INVALID_VALUE:
            error_name = "GLFW_INVALID_VALUE";
            break;
        case GLFW_OUT_OF_MEMORY:
            error_name = "GLFW_OUT_OF_MEMORY";
            break;
        case GLFW_API_UNAVAILABLE:
            error_name = "GLFW_API_UNAVAILABLE";
            break;
        case GLFW_VERSION_UNAVAILABLE:
            error_name = "GLFW_VERSION_UNAVAILABLE";
            break;
        case GLFW_PLATFORM_ERROR:
            error_name = "GLFW_PLATFORM_ERROR";
            break;
        case GLFW_FORMAT_UNAVAILABLE:
            error_name = "GLFW_FORMAT_UNAVAILABLE";
            break;
        default: error_name = "GLFW_ERROR_UNKNOWN: " + error;
    }

    LOG(CRITICAL, error_name << ": " << description);
}

void Window::on_window_resized(GLFWwindow *glfw_window, const int width, const int height) {
    Window *window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(glfw_window));
    window->dimensions = vec2_ui(static_cast<unsigned>(width), static_cast<unsigned>(height));
    window->resized_ = true;
}

void Window::on_window_closed(GLFWwindow *glfw_window) {
    Window *window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(glfw_window));
    window->should_close = true;
}

void Window::debug(const std::string &title, const vec2_ui &dimensions, const bool borderless, const vec2_ui &pos,
                   const bool resizable) {
    DebugUtils::debug_table(DEBUG, 0,
                            "title", title,
                            "dimension", to_string(dimensions),
                            "borderless", borderless,
                            "position", to_string(pos),
                            "resizable", resizable);
}
}
