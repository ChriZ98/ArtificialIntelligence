﻿#pragma once
#include "stdafx.h"
#include "vulkan/Buffer.h"
#include "vulkan/CommandBuffer.h"
#include "vulkan/CommandPool.h"
#include "vulkan/DebugReportCallback.h"
#include "vulkan/DescriptorPool.h"
#include "vulkan/DescriptorSet.h"
#include "vulkan/DescriptorSetLayout.h"
#include "vulkan/Device.h"
#include "vulkan/Framebuffer.h"
#include "vulkan/ImageView.h"
#include "vulkan/Instance.h"
#include "vulkan/PhysicalDevice.h"
#include "vulkan/Pipeline.h"
#include "vulkan/RenderPass.h"
#include "vulkan/Semaphore.h"
#include "vulkan/Surface.h"
#include "vulkan/SwapChain.h"
#include "UniformBufferObject.h"
#include "Vertex.h"
#include "Window.h"

namespace zindach_game_engine {

class GraphicsCore {
public:
#ifndef NDEBUG
    std::vector<const char*> validation_layers;
#endif
    std::vector<const char*> device_extensions;
    std::vector<Vertex> vertices;
    std::vector<uint16_t> indices;
    UniformBufferObject ubo;

    Window &window;
    Instance instance;
#ifndef NDEBUG
    DebugReportCallback debug_report_callback;
#endif
    Surface surface;
    PhysicalDevice physical_device;
    Device device;
    SwapChain swap_chain;
    std::vector<ImageView> image_views;
    RenderPass render_pass;
    DescriptorSetLayout descriptor_set_layout;
    Pipeline pipeline;
    std::vector<Framebuffer> framebuffers;
    CommandPool command_pool;
    Buffer vertex_buffer;
    Buffer index_buffer;
    Buffer uniform_buffer;
    DescriptorPool descriptor_pool;
    DescriptorSet descriptor_set;
    std::vector<CommandBuffer> command_buffers;
    Semaphore image_available;
    Semaphore render_finished;

    explicit GraphicsCore(Window &window);

    void clean_up();
    void clean_up_swap_chain();
    void recreate_swap_chain();
    void render();
    void update_uniform_buffer();
};
}
