﻿#pragma once
#include "stdafx.h"

namespace zindach_game_engine {

class Vertex {
public:
    zindach_math::vec2_f pos;
    zindach_math::vec3_f color;

    Vertex(const zindach_math::vec2_f &pos, const zindach_math::vec3_f &color);

    static vk::VertexInputBindingDescription get_binding_description();
    static std::array<vk::VertexInputAttributeDescription, 2> get_attribute_descriptions();
};
}
