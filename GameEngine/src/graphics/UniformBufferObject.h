﻿#pragma once

namespace zindach_game_engine {

class UniformBufferObject {
public:
    zindach_math::mat4_f model;
    zindach_math::mat4_f view;
    zindach_math::mat4_f projection;
    zindach_math::mat4_f mvp;

    UniformBufferObject(const zindach_math::mat4_f &model, const zindach_math::mat4_f &view,
                        const zindach_math::mat4_f &projection);

    void *get_mvp();
};
}
