﻿#pragma once
#include "stdafx.h"

namespace zindach_game_engine {
class GraphicsCore;

template <typename T>
class VulkanWrapper : public T {
protected:
    GraphicsCore *graphics_core_;

    template <typename R>
    void assign(R &&other) {
        T::operator=(other);
    }

public:
    explicit VulkanWrapper(GraphicsCore &graphics_core)
        : graphics_core_(&graphics_core) { }

    T &operator*() {
        return *this;
    }

    T &operator*() const {
        return *this;
    }
};
}
