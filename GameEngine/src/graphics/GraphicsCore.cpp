﻿#include "stdafx.h"
#include "GraphicsCore.h"
#include "core/Logger.h"

namespace zindach_game_engine {

GraphicsCore::GraphicsCore(Window &window)
    :
#ifndef NDEBUG
    validation_layers({"VK_LAYER_LUNARG_standard_validation"}),
#endif
    device_extensions({VK_KHR_SWAPCHAIN_EXTENSION_NAME}),
    vertices({
        Vertex(vec2_f(-0.5f, -0.5f), vec3_f(1.0f, 0.0f, 0.0f)) ,
        Vertex(vec2_f(0.5f, -0.5f), vec3_f(0.0f, 1.0f, 0.0f)) ,
        Vertex(vec2_f(0.5f, 0.5f), vec3_f(0.0f, 0.0f, 1.0f)),
        Vertex(vec2_f(-0.5f, 0.5f), vec3_f(1.0f, 1.0f, 1.0f))
    }),
    indices({0,1,2,2,3,0}),

    ubo(mat4_f(), view_projection(vec3_f(1.0f, 1.0f, 1.0f), vec3_f(0.0f, 0.0f, 0.0f), vec3_f(0.0f, 0.0f, 1.0f)),
        perspective_projection(60.0f, 0.01f, 10.0f, static_cast<float>(window.dimensions(0)),
                               static_cast<float>(window.dimensions(1)))),
    window(window),
    instance(*this),
#ifndef NDEBUG
    debug_report_callback(*this),
#endif
    surface(*this),
    physical_device(*this),
    device(*this),
    swap_chain(*this),
    image_views(ImageView::create_vector(*this)),
    render_pass(*this),
    descriptor_set_layout(*this),
    pipeline(*this),
    framebuffers(Framebuffer::create_vector(*this)),
    command_pool(*this),
    vertex_buffer(Buffer::create_and_upload_buffer(*this, device, vertices, vk::BufferUsageFlagBits::eVertexBuffer)),
    index_buffer(Buffer::create_and_upload_buffer(*this, device, indices, vk::BufferUsageFlagBits::eIndexBuffer)),
    uniform_buffer(*this, sizeof(mat4_f), vk::BufferUsageFlagBits::eUniformBuffer,
                   vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent),
    descriptor_pool(*this),
    descriptor_set(*this),
    command_buffers(CommandBuffer::create_vector(*this)),
    image_available(*this),
    render_finished(*this) {
    LOG(INFO, "Initialized graphics core");
    window.show();
}

void GraphicsCore::clean_up() {
    window.hide();
    device.waitIdle();

    clean_up_swap_chain();

    descriptor_pool.destroy();
    descriptor_set_layout.destroy();
    uniform_buffer.destroy();
    index_buffer.destroy();
    vertex_buffer.destroy();
    render_finished.destroy();
    image_available.destroy();
    command_pool.destroy();
    device.destroy();
#ifndef NDEBUG
    debug_report_callback.destroy();
#endif
    surface.destroy();
    instance.destroy();
    window.destroy();

    LOG(DEBUG, "Graphics core destroyed");
}

void GraphicsCore::clean_up_swap_chain() {
    Framebuffer::destroy_vector(framebuffers);
    CommandBuffer::destroy_vector(command_buffers);
    pipeline.destroy();
    render_pass.destroy();
    ImageView::destroy_vector(image_views);
    swap_chain.destroy();
}

void GraphicsCore::recreate_swap_chain() {
    device.waitIdle();

    clean_up_swap_chain();

    swap_chain = SwapChain(*this);
    image_views = ImageView::create_vector(*this);
    render_pass = RenderPass(*this);
    pipeline = Pipeline(*this);
    framebuffers = Framebuffer::create_vector(*this);
    command_buffers = CommandBuffer::create_vector(*this);
}

void GraphicsCore::render() {
    window.update();

    if (window.dimensions == vec2_ui(0u, 0u)) {
        return;
    }

#ifndef NDEBUG
    device.present_queue.waitIdle();
#endif

    if (window.resized()) {
        recreate_swap_chain();
    }

    uint32_t image_index = device.acquireNextImageKHR(*swap_chain, std::numeric_limits<uint64_t>::max(),
                                                      *image_available, nullptr).value;

    std::array<vk::PipelineStageFlags, 1> wait_stages{vk::PipelineStageFlagBits::eColorAttachmentOutput};

    const vk::SubmitInfo submit_info(1, &image_available, wait_stages.data(), 1, &command_buffers[image_index], 1,
                                     &render_finished);

    device.graphics_queue.submit(submit_info, nullptr);

    const vk::PresentInfoKHR present_info(1, &render_finished, 1, &swap_chain, &image_index);

    device.present_queue.presentKHR(present_info);
}

void GraphicsCore::update_uniform_buffer() {
    ubo.model = ubo.model * rotation_matrix(vec3_f(0.0f, 0.0f, 1.0f), 0.0005f);
    void *data = device.mapMemory(uniform_buffer.buffer_memory, 0, sizeof mat4_f);
    memcpy(data, ubo.get_mvp(), sizeof mat4_f);
    device.unmapMemory(uniform_buffer.buffer_memory);
}
}
