﻿#include "stdafx.h"
#include "DebugReportCallback.h"
#include "core/DebugUtils.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

DebugReportCallback::DebugReportCallback(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Init debug callback");

    vk::DebugReportCallbackCreateInfoEXT create_info;
    create_info.flags = vk::DebugReportFlagBitsEXT::eError |
                        vk::DebugReportFlagBitsEXT::eWarning |
                        vk::DebugReportFlagBitsEXT::ePerformanceWarning;
    create_info.pfnCallback = debug_callback;

    debug(create_info);
    assign(graphics_core.instance.createDebugReportCallbackEXT(create_info));
}

void DebugReportCallback::destroy() {
    graphics_core_->instance.destroyDebugReportCallbackEXT(**this);
}

VkBool32 DebugReportCallback::debug_callback(const VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT object_type,
                                             uint64_t object, size_t location, int32_t message_code,
                                             const char *layer_prefix, const char *message, void *user_data) {
    LogSeverity severity;
    switch (flags) {
        case VK_DEBUG_REPORT_INFORMATION_BIT_EXT:
            severity = INFO;
            break;
        case VK_DEBUG_REPORT_WARNING_BIT_EXT:
        case VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT:
            severity = WARNING;
            break;
        case VK_DEBUG_REPORT_ERROR_BIT_EXT:
            severity = CRITICAL;
            break;
        case VK_DEBUG_REPORT_DEBUG_BIT_EXT:
        default:
            severity = DEBUG;
    }

    LOG(severity, "Vulkan Message\n\t\tobj type: " << object_type << "\n\t\tobj: " << object << "\n\t\tlocation: " <<
        location << "\n\t\tmessage code: " << message_code << "\n\t\tlayer prefix: " << layer_prefix << "\n\t\tuser data: "
        << user_data << "\n\t\tmessage:\n" << message);

    return VK_FALSE;
}

void DebugReportCallback::debug(const vk::DebugReportCallbackCreateInfoEXT &create_info) {
    DebugUtils::debug_table(DEBUG, 0,
        "flags", to_string(create_info.flags),
        "user data", create_info.pUserData);
}
}
