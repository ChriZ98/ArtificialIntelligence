﻿#include "stdafx.h"
#include "RenderPass.h"
#include "core/DebugUtils.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

RenderPass::RenderPass(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Create render pass");

    vk::AttachmentDescription color_attachment;
    color_attachment.format = graphics_core.swap_chain.image_format;
    color_attachment.samples = vk::SampleCountFlagBits::e1;
    color_attachment.loadOp = vk::AttachmentLoadOp::eClear;
    color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
    color_attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    color_attachment.initialLayout = vk::ImageLayout::eUndefined;
    color_attachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

    vk::AttachmentReference color_attachment_ref;
    color_attachment_ref.layout = vk::ImageLayout::eColorAttachmentOptimal;

    vk::SubpassDescription subpass;
    subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &color_attachment_ref;

    vk::SubpassDependency dependency;
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;

    vk::RenderPassCreateInfo create_info;
    create_info.attachmentCount = 1;
    create_info.pAttachments = &color_attachment;
    create_info.subpassCount = 1;
    create_info.pSubpasses = &subpass;
    create_info.dependencyCount = 1;
    create_info.pDependencies = &dependency;

    debug(create_info);
    assign(graphics_core.device.createRenderPass(create_info));
}

void RenderPass::destroy() {
    graphics_core_->device.destroyRenderPass(**this);
}

void RenderPass::debug(const vk::RenderPassCreateInfo &create_info) {
    LOG_ADD(DEBUG, "attachments:");
    for (uint32_t i = 0; i < create_info.attachmentCount; ++i) {
        LOG_ADD(DEBUG, "    attachment #" << i << ":");
        DebugUtils::debug_table(DEBUG, 8,
            "format", to_string(create_info.pAttachments[i].format),
            "samples", to_string(create_info.pAttachments[i].samples),
            "load op", to_string(create_info.pAttachments[i].loadOp),
            "initial layout", to_string(create_info.pAttachments[i].initialLayout),
            "final layout", to_string(create_info.pAttachments[i].finalLayout));
    }

    LOG_ADD(DEBUG, "subpasses:");
    for (uint32_t i = 0; i < create_info.subpassCount; ++i) {
        LOG_ADD(DEBUG, "    subpass #" << i << ":");
        DebugUtils::debug_table(DEBUG, 8, "pipeline bind point", to_string(create_info.pSubpasses[i].pipelineBindPoint));

        for (uint32_t j = 0; j < create_info.pSubpasses[i].colorAttachmentCount; ++j) {
            LOG_ADD(DEBUG, "        color attachment #" << j << ":");
            DebugUtils::debug_table(DEBUG, 12,
                "layout", to_string(create_info.pSubpasses[i].pColorAttachments[j].layout),
                "attachment", create_info.pSubpasses[i].pColorAttachments[j].attachment);
        }
    }

    LOG_ADD(DEBUG, "dependencies:");
    for (uint32_t i = 0; i < create_info.dependencyCount; ++i) {
        LOG_ADD(DEBUG, "    dependency #" << i << ":");
        DebugUtils::debug_table(DEBUG, 8,
            "dependency flags", to_string(create_info.pDependencies[i].dependencyFlags),
            "dst access mask", to_string(create_info.pDependencies[i].dstAccessMask),
            "dst stage mask", to_string(create_info.pDependencies[i].dstStageMask),
            "dst subpass", create_info.pDependencies[i].dstSubpass,
            "src access mask", to_string(create_info.pDependencies[i].srcAccessMask),
            "src stage mask", to_string(create_info.pDependencies[i].srcStageMask),
            "src subpass", create_info.pDependencies[i].srcSubpass);
    }
}
}
