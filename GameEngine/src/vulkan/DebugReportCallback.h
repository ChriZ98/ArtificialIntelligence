﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class DebugReportCallback : public VulkanWrapper<vk::DebugReportCallbackEXT> {
    static VkBool32 debug_callback(const VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT object_type,
                                   uint64_t object, size_t location, int32_t message_code, const char *layer_prefix,
                                   const char *message, void *user_data);
    static void debug(const vk::DebugReportCallbackCreateInfoEXT &create_info);

public:
    explicit DebugReportCallback(GraphicsCore &graphics_core);
    void destroy();
};
}
