﻿#include "stdafx.h"
#include "DescriptorSet.h"
#include "graphics/GraphicsCore.h"
#include "core/Logger.h"

namespace zindach_game_engine {

DescriptorSet::DescriptorSet(GraphicsCore &graphics_core)
    : VulkanWrapper<vk::DescriptorSet>(graphics_core) {
    LOG(INFO, "Create descriptor set");

    vk::DescriptorSetAllocateInfo allocate_info;
    allocate_info.descriptorPool = *graphics_core.descriptor_pool;
    allocate_info.descriptorSetCount = 1;
    allocate_info.pSetLayouts = &graphics_core.descriptor_set_layout;

    assign(graphics_core.device.allocateDescriptorSets(allocate_info)[0]);

    vk::DescriptorBufferInfo buffer_info;
    buffer_info.buffer = *graphics_core.uniform_buffer;
    buffer_info.offset = 0;
    buffer_info.range = sizeof mat4_f;

    vk::WriteDescriptorSet descriptor_write;
    descriptor_write.dstSet = **this;
    descriptor_write.dstBinding = 0;
    descriptor_write.dstArrayElement = 0;
    descriptor_write.descriptorType = vk::DescriptorType::eUniformBuffer;
    descriptor_write.descriptorCount = 1;
    descriptor_write.pBufferInfo = &buffer_info;

    graphics_core.device.updateDescriptorSets(descriptor_write, nullptr);
}
}
