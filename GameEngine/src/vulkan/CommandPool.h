﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class CommandPool : public VulkanWrapper<vk::CommandPool> {
public:
    explicit CommandPool(GraphicsCore &graphics_core);
    void destroy();
};
}
