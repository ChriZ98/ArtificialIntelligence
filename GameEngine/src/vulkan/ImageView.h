﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class ImageView : public VulkanWrapper<vk::ImageView> {
public:
    explicit ImageView(GraphicsCore &graphics_core, const vk::Image &image);
    void destroy();

    static std::vector<ImageView> create_vector(GraphicsCore &graphics_core);
    static void destroy_vector(std::vector<ImageView> &image_views);
};
}
