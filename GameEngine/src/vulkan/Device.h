﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class Device : public VulkanWrapper<vk::Device> {
    static void debug(const VkDeviceCreateInfo &create_info);

public:
    vk::Queue graphics_queue;
    vk::Queue present_queue;

    explicit Device(GraphicsCore &graphics_core);
    void destroy();
};
}
