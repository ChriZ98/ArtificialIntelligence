﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

struct SwapChainSupportDetail {
    vk::SurfaceCapabilitiesKHR capabilities;
    std::vector<vk::SurfaceFormatKHR> formats;
    std::vector<vk::PresentModeKHR> present_modes;

    SwapChainSupportDetail() {}

    SwapChainSupportDetail(const vk::PhysicalDevice &physical_device, const vk::SurfaceKHR &surface)
        : capabilities(physical_device.getSurfaceCapabilitiesKHR(surface)),
          formats(physical_device.getSurfaceFormatsKHR(surface)),
          present_modes(physical_device.getSurfacePresentModesKHR(surface)) { }
};

struct QueueFamilyIndices {
    int graphics_family = -1;
    int present_family = -1;

    bool is_complete() const {
        return graphics_family >= 0 && present_family >= 0;
    }
};

class PhysicalDevice : public VulkanWrapper<vk::PhysicalDevice> {
    QueueFamilyIndices find_queue_families(const vk::PhysicalDevice &physical_device) const;
    bool is_device_suitable(const vk::PhysicalDevice &physical_device) const;
    bool check_device_extension_support(const vk::PhysicalDevice &physical_device) const;
    static void debug(const std::vector<vk::PhysicalDevice> &physical_devices);
    static void debug(const vk::PhysicalDevice &physical_device, const SwapChainSupportDetail &support_detail,
                      const std::vector<const char *> &device_extensions);

public:
    SwapChainSupportDetail support_detail;
    QueueFamilyIndices queue_family_indices;

    explicit PhysicalDevice(GraphicsCore &graphics_core);

    void refresh();
};
}
