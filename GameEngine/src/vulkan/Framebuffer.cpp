﻿#include "stdafx.h"
#include "Framebuffer.h"
#include "core/Logger.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

Framebuffer::Framebuffer(GraphicsCore &graphics_core, const vk::ImageView &image_view)
    : VulkanWrapper(graphics_core) {
    std::array<vk::ImageView, 1> attachments{image_view};

    vk::FramebufferCreateInfo create_info;
    create_info.renderPass = *graphics_core.render_pass;
    create_info.attachmentCount = static_cast<uint32_t>(attachments.size());
    create_info.pAttachments = attachments.data();
    create_info.width = graphics_core.swap_chain.extent.width;
    create_info.height = graphics_core.swap_chain.extent.height;
    create_info.layers = 1;

    assign(graphics_core.device.createFramebuffer(create_info));
}

void Framebuffer::destroy() {
    graphics_core_->device.destroyFramebuffer(**this);
}

std::vector<Framebuffer> Framebuffer::create_vector(GraphicsCore &graphics_core) {
    LOG(INFO, "Create framebuffers");

    std::vector<Framebuffer> result;
    for (auto &image_view : graphics_core.image_views) {
        result.emplace_back(graphics_core, image_view);
    }

    return result;
}

void Framebuffer::destroy_vector(std::vector<Framebuffer> &framebuffers) {
    for (auto &framebuffer : framebuffers) {
        framebuffer.destroy();
    }
}
}
