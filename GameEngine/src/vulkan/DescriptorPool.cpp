﻿#include "stdafx.h"
#include "DescriptorPool.h"
#include "graphics/GraphicsCore.h"
#include "core/Logger.h"

namespace zindach_game_engine {

DescriptorPool::DescriptorPool(GraphicsCore &graphics_core)
    : VulkanWrapper<vk::DescriptorPool>(graphics_core) {
    LOG(INFO, "Create descriptor pool");
    vk::DescriptorPoolSize pool_size(vk::DescriptorType::eUniformBuffer, 1);

    vk::DescriptorPoolCreateInfo pool_info;
    pool_info.poolSizeCount = 1;
    pool_info.pPoolSizes = &pool_size;
    pool_info.maxSets = 1;

    assign(graphics_core.device.createDescriptorPool(pool_info));
}

void DescriptorPool::destroy() {
    graphics_core_->device.destroyDescriptorPool(**this);
}
}
