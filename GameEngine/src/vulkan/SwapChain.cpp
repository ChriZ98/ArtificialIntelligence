﻿#include "stdafx.h"
#include "SwapChain.h"
#include "core/DebugUtils.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

SwapChain::SwapChain(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Create swap chain");

    auto &capabilities = graphics_core_->physical_device.support_detail.capabilities;
    graphics_core.physical_device.refresh();

    const vk::SurfaceFormatKHR surface_format = choose_swap_surface_format();
    const vk::PresentModeKHR present_mode = choose_swap_present_mode();
    extent = choose_swap_extent();

    uint32_t image_count = capabilities.minImageCount + 1;
    if (capabilities.maxImageCount > 0 && image_count > capabilities.maxImageCount) {
        image_count = capabilities.maxImageCount;
    }

    vk::SwapchainCreateInfoKHR create_info;
    create_info.surface = *graphics_core.surface;
    create_info.minImageCount = image_count;
    create_info.imageFormat = surface_format.format;
    create_info.imageColorSpace = surface_format.colorSpace;
    create_info.imageExtent = extent;
    create_info.imageArrayLayers = 1;
    create_info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;
    create_info.preTransform = capabilities.currentTransform;
    create_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    create_info.presentMode = present_mode;
    create_info.clipped = VK_TRUE;

    auto &indices = graphics_core.physical_device.queue_family_indices;
    if (indices.graphics_family != indices.present_family) {
        std::array<uint32_t, 2> queue_family_indices{
            static_cast<uint32_t>(indices.graphics_family),
            static_cast<uint32_t>(indices.present_family)
        };

        create_info.imageSharingMode = vk::SharingMode::eConcurrent;
        create_info.queueFamilyIndexCount = static_cast<uint32_t>(queue_family_indices.size());
        create_info.pQueueFamilyIndices = queue_family_indices.data();
    }

    debug(create_info);
    assign(graphics_core.device.createSwapchainKHR(create_info));

    images = graphics_core.device.getSwapchainImagesKHR(**this);
    image_format = surface_format.format;
}

void SwapChain::destroy() {
    graphics_core_->device.destroySwapchainKHR(**this);
}

vk::SurfaceFormatKHR SwapChain::choose_swap_surface_format() const {
    auto &formats = graphics_core_->physical_device.support_detail.formats;
    if (formats.size() == 1 && formats[0].format == vk::Format
        ::eUndefined) {
        return {vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear};
    }

    for (const auto &available_format : formats) {
        if (available_format.format == vk::Format::eB8G8R8A8Unorm && available_format.colorSpace == vk::ColorSpaceKHR::
            eSrgbNonlinear) {
            return available_format;
        }
    }

    return formats[0];
}

vk::PresentModeKHR SwapChain::choose_swap_present_mode() const {
    vk::PresentModeKHR best_mode = vk::PresentModeKHR::eFifo;

    for (const auto &available_present_mode : graphics_core_->physical_device.support_detail.present_modes) {
        if (available_present_mode == vk::PresentModeKHR::eMailbox) {
            return available_present_mode;
        }
        if (available_present_mode == vk::PresentModeKHR::eImmediate) {
            best_mode = available_present_mode;
        }
    }

    return best_mode;
}

vk::Extent2D SwapChain::choose_swap_extent() const {
    auto &capabilities = graphics_core_->physical_device.support_detail.capabilities;
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::
        max()) {
        return capabilities.currentExtent;
    }

    vk::Extent2D actual_extend = {graphics_core_->window.dimensions(0), graphics_core_->window.dimensions(1)};

    actual_extend.width = std::max(capabilities.minImageExtent.width,
        std::min(capabilities.maxImageExtent.width, actual_extend.width));
    actual_extend.height = std::max(capabilities.minImageExtent.height,
        std::min(capabilities.maxImageExtent.height, actual_extend.height));

    return actual_extend;
}

void SwapChain::debug(const vk::SwapchainCreateInfoKHR &create_info) {
    DebugUtils::debug_table(DEBUG, 0,
        "image sharing mode", to_string(create_info.imageSharingMode),
        "clipped", create_info.clipped,
        "composite alpha", to_string(create_info.compositeAlpha),
        "image array layers", create_info.imageArrayLayers,
        "image color space", to_string(create_info.imageColorSpace),
        "image extent", DebugUtils::debug_extent(create_info.imageExtent),
        "image format", to_string(create_info.imageFormat),
        "image usage", to_string(create_info.imageUsage),
        "min image count", create_info.minImageCount,
        "pre transform", to_string(create_info.preTransform),
        "present mode", to_string(create_info.presentMode),
        "family index count", create_info.queueFamilyIndexCount);
}
}
