﻿#include "stdafx.h"
#include "Buffer.h"
#include "core/Logger.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

Buffer::Buffer(GraphicsCore &graphics_core, const vk::DeviceSize size, const vk::BufferUsageFlags usage,
               const vk::MemoryPropertyFlags properties)
    : VulkanWrapper(graphics_core) {
    vk::BufferCreateInfo buffer_info;
    buffer_info.size = size;
    buffer_info.usage = usage;

    assign(graphics_core.device.createBuffer(buffer_info));

    const vk::MemoryRequirements memory_requirements = graphics_core.device.getBufferMemoryRequirements(**this);

    vk::MemoryAllocateInfo allocate_info;
    allocate_info.allocationSize = memory_requirements.size;
    allocate_info.memoryTypeIndex = find_memory_type(memory_requirements.memoryTypeBits, properties);

    buffer_memory = graphics_core.device.allocateMemory(allocate_info);

    graphics_core.device.bindBufferMemory(**this, buffer_memory, 0);
}

void Buffer::destroy() {
    graphics_core_->device.destroyBuffer(**this);
    graphics_core_->device.freeMemory(buffer_memory);
}

uint32_t Buffer::find_memory_type(const uint32_t type_filter, const vk::MemoryPropertyFlags properties) const {
    const vk::PhysicalDeviceMemoryProperties memory_properties = graphics_core_->physical_device.getMemoryProperties();

    for (uint32_t i = 0; i < memory_properties.memoryTypeCount; ++i) {
        if (type_filter & 1 << i && (memory_properties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    LOG(ERROR, "Failed to find suitable memory type!");
}

void Buffer::copy_buffer(const vk::Buffer &source, const vk::DeviceSize size) {
    vk::CommandBufferAllocateInfo allocate_info;
    allocate_info.commandPool = *graphics_core_->command_pool;
    allocate_info.commandBufferCount = 1;

    vk::CommandBuffer command_buffer = graphics_core_->device.allocateCommandBuffers(allocate_info)[0];

    vk::CommandBufferBeginInfo begin_info;
    begin_info.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;

    command_buffer.begin(begin_info);

    vk::BufferCopy copy_region;
    copy_region.size = size;

    command_buffer.copyBuffer(source, **this, copy_region);

    command_buffer.end();

    vk::SubmitInfo submit_info;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;

    graphics_core_->device.graphics_queue.submit(submit_info, nullptr);
    graphics_core_->device.graphics_queue.waitIdle();

    graphics_core_->device.freeCommandBuffers(*graphics_core_->command_pool, command_buffer);
}
}
