﻿#include "stdafx.h"
#include "Surface.h"
#include "core/Logger.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

Surface::Surface(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Create surface");

    VkSurfaceKHR surface_khr;
    vk::Result result = static_cast<vk::Result>(glfwCreateWindowSurface(graphics_core.instance,
        graphics_core.window.glfw_window, nullptr, &surface_khr));

    if (result != vk::Result::eSuccess) {
        LOG(ERROR, "Failed to create GLFW Window Surface. Error: " << vk::to_string(result));
    }

    assign(surface_khr);
}

void Surface::destroy() {
    graphics_core_->instance.destroySurfaceKHR(**this);
}
}
