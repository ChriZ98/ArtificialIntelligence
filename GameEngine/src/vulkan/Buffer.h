﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class Buffer : public VulkanWrapper<vk::Buffer> {
    uint32_t find_memory_type(const uint32_t type_filter, const vk::MemoryPropertyFlags properties) const;
    void copy_buffer(const vk::Buffer &source, const vk::DeviceSize size);

public:
    vk::DeviceMemory buffer_memory;

    Buffer(GraphicsCore &graphics_core, const vk::DeviceSize size, const vk::BufferUsageFlags usage,
           const vk::MemoryPropertyFlags properties);
    void destroy();

    template <typename T>
    static Buffer create_and_upload_buffer(GraphicsCore &graphics_core, const vk::Device &device,
                                           const std::vector<T> &data_vector, const vk::BufferUsageFlags usage);
};

template <typename T>
Buffer Buffer::create_and_upload_buffer(GraphicsCore &graphics_core, const vk::Device &device,
                                        const std::vector<T> &data_vector, const vk::BufferUsageFlags usage) {
    const vk::DeviceSize buffer_size = sizeof T * data_vector.size();

    Buffer staging_buffer(graphics_core, buffer_size, vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    void *data = device.mapMemory(staging_buffer.buffer_memory, 0, buffer_size);
    memcpy(data, data_vector.data(), buffer_size);
    device.unmapMemory(staging_buffer.buffer_memory);

    Buffer buffer(graphics_core, buffer_size,
        vk::BufferUsageFlagBits::eTransferDst | usage,
        vk::MemoryPropertyFlagBits::eDeviceLocal);

    buffer.copy_buffer(staging_buffer, buffer_size);

    staging_buffer.destroy();

    return buffer;
}
}
