﻿#include "stdafx.h"
#include "CommandBuffer.h"
#include "core/Logger.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

CommandBuffer::CommandBuffer(GraphicsCore &graphics_core, const vk::CommandBuffer &command_buffer,
                             const vk::CommandBufferBeginInfo &begin_info, const vk::ClearValue &clear_color,
                             const vk::Framebuffer &framebuffer)
    : VulkanWrapper(graphics_core) {
    assign(command_buffer);

    begin(begin_info);

    vk::RenderPassBeginInfo render_pass_begin_info;
    render_pass_begin_info.renderPass = *graphics_core.render_pass;
    render_pass_begin_info.framebuffer = framebuffer;
    render_pass_begin_info.renderArea.offset = vk::Offset2D();
    render_pass_begin_info.renderArea.extent = graphics_core.swap_chain.extent;
    render_pass_begin_info.clearValueCount = 1;
    render_pass_begin_info.pClearValues = &clear_color;

    beginRenderPass(render_pass_begin_info, vk::SubpassContents::eInline);

    bindPipeline(vk::PipelineBindPoint::eGraphics, *graphics_core.pipeline);

    if (graphics_core.window.resizable) {
        vk::Viewport viewport(0, 0, static_cast<float>(graphics_core.window.dimensions(0)),
            static_cast<float>(graphics_core.window.dimensions(1)), 0,
            1);
        setViewport(0, 1, &viewport);

        vk::Rect2D scissor({0, 0}, {graphics_core.window.dimensions(0), graphics_core.window.dimensions(1)});
        setScissor(0, 1, &scissor);
    }

    std::array<vk::DeviceSize, 1> offsets = {0};
    bindVertexBuffers(0, graphics_core.vertex_buffer, offsets);

    bindIndexBuffer(*graphics_core.index_buffer, 0, vk::IndexType::eUint16);

    bindDescriptorSets(vk::PipelineBindPoint::eGraphics, graphics_core.pipeline.layout, 0, graphics_core.descriptor_set, nullptr);

    drawIndexed(static_cast<uint32_t>(graphics_core.indices.size()), 1, 0, 0, 0);

    endRenderPass();

    end();
}

void CommandBuffer::destroy() const {
    graphics_core_->device.freeCommandBuffers(*graphics_core_->command_pool, *this);
}

std::vector<CommandBuffer> CommandBuffer::create_vector(GraphicsCore &graphics_core) {
    LOG(INFO, "Create commandbuffers");

    std::vector<vk::CommandBuffer> vk_command_buffers(graphics_core.framebuffers.size());

    vk::CommandBufferAllocateInfo allocate_info;
    allocate_info.commandPool = *graphics_core.command_pool;
    allocate_info.level = vk::CommandBufferLevel::ePrimary;
    allocate_info.commandBufferCount = static_cast<uint32_t>(vk_command_buffers.size());

    vk_command_buffers = graphics_core.device.allocateCommandBuffers(allocate_info);

    vk::CommandBufferBeginInfo begin_info;
    begin_info.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse;

    vk::ClearValue clear_color(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f});

    std::vector<CommandBuffer> command_buffers;
    for (size_t i = 0; i < vk_command_buffers.size(); ++i) {
        command_buffers.emplace_back(graphics_core, vk_command_buffers[i], begin_info, clear_color,
            graphics_core.framebuffers[i]);
    }

    return command_buffers;
}

void CommandBuffer::destroy_vector(const std::vector<CommandBuffer> &command_buffers) {
    for (auto &command_buffer : command_buffers) {
        command_buffer.destroy();
    }
}
}
