﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class Surface : public VulkanWrapper<vk::SurfaceKHR> {
public:
    explicit Surface(GraphicsCore &graphics_core);
    void destroy();
};
}
