﻿#include "stdafx.h"
#include "Pipeline.h"
#include "core/DebugUtils.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

Pipeline::Pipeline(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    std::array<vk::PipelineShaderStageCreateInfo, 2> shader_stages{
        create_shader_stage("res/shader/shader.vert.spv", vk::ShaderStageFlagBits::eVertex),
        create_shader_stage("res/shader/shader.frag.spv", vk::ShaderStageFlagBits::eFragment)
    };
    LOG(INFO, "Create graphics pipeline");

    auto binding_description = Vertex::get_binding_description();
    auto attribute_descriptions = Vertex::get_attribute_descriptions();

    vk::PipelineVertexInputStateCreateInfo vertex_input_info;
    vertex_input_info.vertexBindingDescriptionCount = 1;
    vertex_input_info.pVertexBindingDescriptions = &binding_description;
    vertex_input_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribute_descriptions.size());
    vertex_input_info.pVertexAttributeDescriptions = attribute_descriptions.data();

    vk::PipelineInputAssemblyStateCreateInfo input_assembly;
    input_assembly.topology = vk::PrimitiveTopology::eTriangleList;
    input_assembly.primitiveRestartEnable = VK_FALSE;

    vk::Rect2D scissor({0,0}, graphics_core.swap_chain.extent);
    vk::Viewport viewport(0, 0, static_cast<float>(graphics_core.swap_chain.extent.width),
        static_cast<float>(graphics_core.swap_chain.extent.height),
        0, 1);

    vk::PipelineViewportStateCreateInfo viewport_state;
    viewport_state.viewportCount = 1;
    viewport_state.pViewports = &viewport;
    viewport_state.scissorCount = 1;
    viewport_state.pScissors = &scissor;

    vk::PipelineRasterizationStateCreateInfo rasterizer;
    rasterizer.polygonMode = vk::PolygonMode::eFill;
    rasterizer.cullMode = vk::CullModeFlagBits::eBack;
    rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
    rasterizer.lineWidth = 1;

    vk::PipelineMultisampleStateCreateInfo multisampling;

    vk::PipelineColorBlendAttachmentState color_blend_attachment;
    color_blend_attachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG
                                            | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;

    vk::PipelineColorBlendStateCreateInfo color_blending;
    color_blending.attachmentCount = 1;
    color_blending.pAttachments = &color_blend_attachment;

    vk::PipelineLayoutCreateInfo pipeline_layout_info;
    pipeline_layout_info.setLayoutCount = 1;
    pipeline_layout_info.pSetLayouts = &graphics_core.descriptor_set_layout;

    layout = graphics_core.device.createPipelineLayout(pipeline_layout_info);

    vk::GraphicsPipelineCreateInfo pipeline_info;
    pipeline_info.stageCount = static_cast<uint32_t>(shader_stages.size());
    pipeline_info.pStages = shader_stages.data();
    pipeline_info.pVertexInputState = &vertex_input_info;
    pipeline_info.pInputAssemblyState = &input_assembly;
    pipeline_info.pViewportState = &viewport_state;
    pipeline_info.pRasterizationState = &rasterizer;
    pipeline_info.pMultisampleState = &multisampling;
    pipeline_info.pColorBlendState = &color_blending;
    pipeline_info.layout = layout;
    pipeline_info.renderPass = *graphics_core.render_pass;

    if (graphics_core.window.resizable) {
        std::array<vk::DynamicState, 2> dynamic_states{vk::DynamicState::eViewport, vk::DynamicState::eScissor};

        vk::PipelineDynamicStateCreateInfo dynamic_state_info;
        dynamic_state_info.dynamicStateCount = static_cast<uint32_t>(dynamic_states.size());
        dynamic_state_info.pDynamicStates = dynamic_states.data();

        pipeline_info.pDynamicState = &dynamic_state_info;
    }

    debug(pipeline_info);

    assign(graphics_core.device.createGraphicsPipeline(nullptr, pipeline_info));

    for (auto &shader_stage : shader_stages) {
        graphics_core.device.destroyShaderModule(shader_stage.module);
    }
}

void Pipeline::destroy() {
    graphics_core_->device.destroyPipeline(**this);
    graphics_core_->device.destroyPipelineLayout(layout);
}

vk::PipelineShaderStageCreateInfo Pipeline::create_shader_stage(const std::string &filename,
                                                                const vk::ShaderStageFlagBits shader_type) const {
    LOG(DEBUG, "Reading shader file: " << filename);
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    if (!file.is_open()) {
        LOG(ERROR, "Shader file could not be opened")
    }

    const size_t file_size = static_cast<size_t>(file.tellg());
    std::vector<char> buffer(file_size);
    file.seekg(0);
    file.read(buffer.data(), file_size);

    vk::ShaderModuleCreateInfo create_info;
    create_info.codeSize = buffer.size();
    create_info.pCode = reinterpret_cast<const uint32_t*>(buffer.data());

    vk::PipelineShaderStageCreateInfo shader_stage;
    shader_stage.stage = shader_type;
    shader_stage.module = graphics_core_->device.createShaderModule(create_info);
    shader_stage.pName = "main";

    return shader_stage;
}

void Pipeline::debug(const vk::GraphicsPipelineCreateInfo &create_info) {
    if (create_info.pStages) {
        LOG_ADD(DEBUG, "shader stages:");
        for (uint32_t i = 0; i < create_info.stageCount; ++i) {
            LOG_ADD(DEBUG, "    " << to_string(create_info.pStages[i].stage));
        }
    }

    if (create_info.pInputAssemblyState) {
        LOG_ADD(DEBUG, "input assembly state:");
        DebugUtils::debug_table(DEBUG, 4,
            "topology", to_string(create_info.pInputAssemblyState->topology),
            "primitive restart enable", create_info.pInputAssemblyState->primitiveRestartEnable);
    }

    if (create_info.pViewportState) {
        LOG_ADD(DEBUG, "viewport state:");
        DebugUtils::debug_table(DEBUG, 4,
            "viewport max depth", create_info.pViewportState->pViewports[0].maxDepth,
            "viewport min depth", create_info.pViewportState->pViewports[0].minDepth,
            "viewport height", create_info.pViewportState->pViewports[0].height,
            "viewport width", create_info.pViewportState->pViewports[0].width,
            "viewport y", create_info.pViewportState->pViewports[0].y,
            "viewport x", create_info.pViewportState->pViewports[0].x,
            "viewport count", create_info.pViewportState->viewportCount,
            "scissor extent", DebugUtils::debug_extent(create_info.pViewportState->pScissors[0].extent),
            "scissor offset", DebugUtils::debug_offset(create_info.pViewportState->pScissors[0].offset),
            "scissor count", create_info.pViewportState->scissorCount);
    }

    if (create_info.pRasterizationState) {
        LOG_ADD(DEBUG, "rasterization:");
        DebugUtils::debug_table(DEBUG, 4,
            "depthClampEnable", create_info.pRasterizationState->depthClampEnable,
            "rasterizerDiscardEnable", create_info.pRasterizationState->rasterizerDiscardEnable,
            "polygonMode", to_string(create_info.pRasterizationState->polygonMode),
            "cullMode", to_string(create_info.pRasterizationState->cullMode),
            "frontFace", to_string(create_info.pRasterizationState->frontFace),
            "depthBiasEnable", create_info.pRasterizationState->depthBiasEnable,
            "depthBiasConstantFactor", create_info.pRasterizationState->depthBiasConstantFactor,
            "depthBiasClamp", create_info.pRasterizationState->depthBiasClamp,
            "depthBiasSlopeFactor", create_info.pRasterizationState->depthBiasSlopeFactor,
            "lineWidth", create_info.pRasterizationState->lineWidth);
    }
}
}
