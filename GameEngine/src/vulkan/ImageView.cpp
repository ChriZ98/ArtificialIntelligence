﻿#include "stdafx.h"
#include "ImageView.h"
#include "core/Logger.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

ImageView::ImageView(GraphicsCore &graphics_core, const vk::Image &image)
    : VulkanWrapper(graphics_core) {
    vk::ImageViewCreateInfo create_info;
    create_info.image = image;
    create_info.viewType = vk::ImageViewType::e2D;
    create_info.format = graphics_core.swap_chain.image_format;
    create_info.subresourceRange = vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

    assign(graphics_core.device.createImageView(create_info));
}

void ImageView::destroy() {
    graphics_core_->device.destroyImageView(**this);
}

std::vector<ImageView> ImageView::create_vector(GraphicsCore &graphics_core) {
    LOG(INFO, "Create image views");

    std::vector<ImageView> result;
    for (auto &image : graphics_core.swap_chain.images) {
        result.emplace_back(graphics_core, image);
    }

    return result;
}

void ImageView::destroy_vector(std::vector<ImageView> &image_views) {
    for (auto &image_view : image_views) {
        image_view.destroy();
    }
}
}
