﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class CommandBuffer : public VulkanWrapper<vk::CommandBuffer> {
public:
    CommandBuffer(GraphicsCore &graphics_core, const vk::CommandBuffer &command_buffer,
                  const vk::CommandBufferBeginInfo &begin_info, const vk::ClearValue &clear_color,
                  const vk::Framebuffer &framebuffer);
    void destroy() const;

    static std::vector<CommandBuffer> create_vector(GraphicsCore &graphics_core);
    static void destroy_vector(const std::vector<CommandBuffer> &command_buffers);
};
}
