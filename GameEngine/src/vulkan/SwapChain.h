﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class SwapChain : public VulkanWrapper<vk::SwapchainKHR> {
    vk::SurfaceFormatKHR choose_swap_surface_format() const;
    vk::PresentModeKHR choose_swap_present_mode() const;
    vk::Extent2D choose_swap_extent() const;
    static void debug(const vk::SwapchainCreateInfoKHR &create_info);

public:
    vk::Extent2D extent;
    vk::Format image_format;
    std::vector<vk::Image> images;

    explicit SwapChain(GraphicsCore &graphics_core);
    void destroy();
};
}
