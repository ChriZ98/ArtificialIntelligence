﻿#include "stdafx.h"
#include "DescriptorSetLayout.h"
#include "graphics/GraphicsCore.h"
#include "core/Logger.h"

namespace zindach_game_engine {

DescriptorSetLayout::DescriptorSetLayout(GraphicsCore &graphics_core):VulkanWrapper(graphics_core) {
    LOG(INFO, "Create descriptor set layout");

    vk::DescriptorSetLayoutBinding ubo_layout_binding;
    ubo_layout_binding.binding = 0;
    ubo_layout_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
    ubo_layout_binding.descriptorCount = 1;
    ubo_layout_binding.stageFlags = vk::ShaderStageFlagBits::eVertex;

    vk::DescriptorSetLayoutCreateInfo layout_info;
    layout_info.bindingCount = 1;
    layout_info.pBindings = &ubo_layout_binding;
       
    assign(graphics_core.device.createDescriptorSetLayout(layout_info));
}

void DescriptorSetLayout::destroy() {
    graphics_core_->device.destroyDescriptorSetLayout(**this);
}
}
