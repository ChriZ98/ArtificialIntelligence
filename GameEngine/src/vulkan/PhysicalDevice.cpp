﻿#include "stdafx.h"
#include "PhysicalDevice.h"
#include "core/DebugUtils.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

PhysicalDevice::PhysicalDevice(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Pick physical device");

    std::vector<vk::PhysicalDevice> physical_devices = graphics_core.instance.enumeratePhysicalDevices();

    if (physical_devices.size() == 0) {
        LOG(ERROR, "Failed to find GPUs with Vulkan support")
    }
    debug(physical_devices);

    for (auto &device : physical_devices) {
        if (is_device_suitable(device)) {
            assign(device);
            break;
        }
    }

    if (operator!()) {
        LOG(ERROR, "Failed to find a suitable GPU");
    }

    queue_family_indices = find_queue_families(graphics_core.physical_device);
    refresh();

    debug(graphics_core.physical_device, support_detail, graphics_core.device_extensions);
}

void PhysicalDevice::refresh() {
    support_detail = SwapChainSupportDetail(graphics_core_->physical_device, graphics_core_->surface);
}

QueueFamilyIndices PhysicalDevice::find_queue_families(const vk::PhysicalDevice &physical_device) const {
    QueueFamilyIndices indices;
    std::vector<vk::QueueFamilyProperties> queue_families = physical_device.getQueueFamilyProperties();

    int i = 0;
    for (auto &queue_family : queue_families) {
        if (queue_family.queueCount > 0 && queue_family.queueFlags & vk::QueueFlagBits::eGraphics) {
            indices.graphics_family = i;
        }
        if (queue_family.queueCount > 0 && physical_device.getSurfaceSupportKHR(i, *graphics_core_->surface)) {
            indices.present_family = i;
        }
        if (indices.is_complete()) {
            break;
        }
        ++i;
    }

    return indices;
}

bool PhysicalDevice::is_device_suitable(const vk::PhysicalDevice &physical_device) const {
    QueueFamilyIndices indices = find_queue_families(physical_device);

    if (check_device_extension_support(physical_device)) {
        SwapChainSupportDetail swap_chain_support(physical_device, graphics_core_->surface);
        return indices.is_complete() && !swap_chain_support.formats.empty() && !swap_chain_support.present_modes.empty();
    }

    return false;
}

bool PhysicalDevice::check_device_extension_support(const vk::PhysicalDevice &physical_device) const {
    std::vector<vk::ExtensionProperties> available_extensions = physical_device.enumerateDeviceExtensionProperties();
    std::set<std::string> required_extensions(graphics_core_->device_extensions.begin(),
        graphics_core_->device_extensions.end());

    for (const auto &extension : available_extensions) {
        required_extensions.erase(extension.extensionName);
    }

    return required_extensions.empty();
}

void PhysicalDevice::debug(const std::vector<vk::PhysicalDevice> &physical_devices) {
    LOG_ADD(DEBUG, "physical devices:");
    for (const auto &physical_device : physical_devices) {
        LOG_ADD(DEBUG, "    " << physical_device << " " << physical_device.getProperties().deviceName);
    }
}

void PhysicalDevice::debug(const vk::PhysicalDevice &physical_device, const SwapChainSupportDetail &support_detail,
                           const std::vector<const char *> &device_extensions) {
    LOG_ADD(DEBUG, "suitable device: " << physical_device);

    const vk::PhysicalDeviceProperties properties = physical_device.getProperties();
    LOG_ADD(DEBUG, "properties:");
    DebugUtils::debug_table(DEBUG, 4,
        "apiVersion", DebugUtils::debug_version(properties.apiVersion),
        "driver version", DebugUtils::debug_version(properties.driverVersion),
        "vendor ID", properties.vendorID,
        "device ID", properties.deviceID,
        "device type", to_string(properties.deviceType),
        "device name", properties.deviceName);

    const vk::PhysicalDeviceMemoryProperties memory = physical_device.getMemoryProperties();
    LOG_ADD(DEBUG, "memory properties:");
    LOG_ADD(DEBUG, "memory heaps:");
    for (uint32_t i = 0; i < memory.memoryHeapCount; ++i) {
        LOG_ADD(DEBUG, "    memory heap #" << i << ":");
        DebugUtils::debug_table(DEBUG, 8,
            "size", memory.memoryHeaps[i].size,
            "flags", to_string(memory.memoryHeaps[i].flags));
    }
    LOG_ADD(VERBOSE, "memory types:");
    for (uint32_t i = 0; i < memory.memoryTypeCount; ++i) {
        LOG_ADD(VERBOSE, "    memory type #" << i << ":");
        DebugUtils::debug_table(VERBOSE, 8,
            "size", memory.memoryTypes[i].heapIndex,
            "flags", to_string(memory.memoryTypes[i].propertyFlags));
    }

    const vk::PhysicalDeviceFeatures features = physical_device.getFeatures();
    LOG_ADD(VERBOSE, "features:");
    DebugUtils::debug_table(VERBOSE, 4,
        "robustBufferAccess", features.robustBufferAccess,
        "fullDrawIndexUint32", features.fullDrawIndexUint32,
        "imageCubeArray", features.imageCubeArray,
        "independentBlend", features.independentBlend,
        "geometryShader", features.geometryShader,
        "tessellationShader", features.tessellationShader,
        "sampleRateShading", features.sampleRateShading,
        "dualSrcBlend", features.dualSrcBlend,
        "logicOp", features.logicOp,
        "multiDrawIndirect", features.multiDrawIndirect,
        "drawIndirectFirstInstance", features.drawIndirectFirstInstance,
        "depthClamp", features.depthClamp,
        "depthBiasClamp", features.depthBiasClamp,
        "fillModeNonSolid", features.fillModeNonSolid,
        "depthBounds", features.depthBounds,
        "wideLines", features.wideLines,
        "largePoints", features.largePoints,
        "alphaToOne", features.alphaToOne,
        "multiViewport", features.multiViewport,
        "samplerAnisotropy", features.samplerAnisotropy,
        "textureCompressionETC2", features.textureCompressionETC2,
        "textureCompressionASTC_LDR", features.textureCompressionASTC_LDR,
        "textureCompressionBC", features.textureCompressionBC,
        "occlusionQueryPrecise", features.occlusionQueryPrecise,
        "pipelineStatisticsQuery", features.pipelineStatisticsQuery,
        "vertexPipelineStoresAndAtomics", features.vertexPipelineStoresAndAtomics,
        "fragmentStoresAndAtomics", features.fragmentStoresAndAtomics,
        "shaderTessellationAndGeometryPointSize", features.shaderTessellationAndGeometryPointSize,
        "shaderImageGatherExtended", features.shaderImageGatherExtended,
        "shaderStorageImageExtendedFormats", features.shaderStorageImageExtendedFormats,
        "shaderStorageImageMultisample", features.shaderStorageImageMultisample,
        "shaderStorageImageReadWithoutFormat", features.shaderStorageImageReadWithoutFormat,
        "shaderStorageImageWriteWithoutFormat", features.shaderStorageImageWriteWithoutFormat,
        "shaderUniformBufferArrayDynamicIndexing", features.shaderUniformBufferArrayDynamicIndexing,
        "shaderSampledImageArrayDynamicIndexing", features.shaderSampledImageArrayDynamicIndexing,
        "shaderStorageBufferArrayDynamicIndexing", features.shaderStorageBufferArrayDynamicIndexing,
        "shaderStorageImageArrayDynamicIndexing", features.shaderStorageImageArrayDynamicIndexing,
        "shaderClipDistance", features.shaderClipDistance,
        "shaderCullDistance", features.shaderCullDistance,
        "shaderFloat64", features.shaderFloat64,
        "shaderInt64", features.shaderInt64,
        "shaderInt16", features.shaderInt16,
        "shaderResourceResidency", features.shaderResourceResidency,
        "shaderResourceMinLod", features.shaderResourceMinLod,
        "sparseBinding", features.sparseBinding,
        "sparseResidencyBuffer", features.sparseResidencyBuffer,
        "sparseResidencyImage2D", features.sparseResidencyImage2D,
        "sparseResidencyImage3D", features.sparseResidencyImage3D,
        "sparseResidency2Samples", features.sparseResidency2Samples,
        "sparseResidency4Samples", features.sparseResidency4Samples,
        "sparseResidency8Samples", features.sparseResidency8Samples,
        "sparseResidency16Samples", features.sparseResidency16Samples,
        "sparseResidencyAliased", features.sparseResidencyAliased,
        "variableMultisampleRate", features.variableMultisampleRate,
        "inheritedQueries", features.inheritedQueries);

    LOG_ADD(DEBUG, "surface capabilities:");
    DebugUtils::debug_table(DEBUG, 4,
        "min image count", support_detail.capabilities.minImageCount,
        "max image count", support_detail.capabilities.maxImageCount,
        "min image extent", DebugUtils::debug_extent(support_detail.capabilities.minImageExtent),
        "max image extent", DebugUtils::debug_extent(support_detail.capabilities.maxImageExtent),
        "current extent", DebugUtils::debug_extent(support_detail.capabilities.currentExtent),
        "max image array layers", support_detail.capabilities.maxImageArrayLayers,
        "supported transforms", to_string(support_detail.capabilities.supportedTransforms),
        "current transforms", to_string(support_detail.capabilities.currentTransform),
        "supported composite alpha", to_string(support_detail.capabilities.supportedCompositeAlpha),
        "supported usage flags", to_string(support_detail.capabilities.supportedUsageFlags)
    );

    LOG_ADD(DEBUG, "surface formats:");
    for (const auto &format : support_detail.formats) {
        LOG_ADD(DEBUG, "    " << vk::to_string(format.format) << "\t" << vk::to_string(format.colorSpace));
    }

    LOG_ADD(DEBUG, "surface present modes:");
    for (const auto &present_mode : support_detail.present_modes) {
        LOG_ADD(DEBUG, "    " << vk::to_string(present_mode));
    }

    LOG_ADD(VERBOSE, "available device extensions:");
    for (auto &entry : physical_device.enumerateDeviceExtensionProperties()) {
        LOG_ADD(VERBOSE, "    " << "spec: " << DebugUtils::debug_version(entry.specVersion) << "\t" << entry.extensionName);
    }

    LOG_ADD(DEBUG, "required device extensions:");
    DebugUtils::debug_array(device_extensions.data(), static_cast<uint32_t>(device_extensions.size()));

    LOG_ADD(DEBUG, "queue families:");
    auto queue_families = physical_device.getQueueFamilyProperties();
    for (size_t i = 0; i < queue_families.size(); ++i) {
        LOG_ADD(DEBUG, "    queue familiy #" << i << ":");
        DebugUtils::debug_table(DEBUG, 8,
            "queue flags", to_string(queue_families[i].queueFlags),
            "min image transfer granularity", DebugUtils::debug_extent(queue_families[i].minImageTransferGranularity),
            "timestamp valid bits", queue_families[i].timestampValidBits,
            "queue count", queue_families[i].queueCount);
    }
}
}
