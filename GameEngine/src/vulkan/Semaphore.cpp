﻿#include "stdafx.h"
#include "Semaphore.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

Semaphore::Semaphore(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    const vk::SemaphoreCreateInfo create_info;

    assign(graphics_core.device.createSemaphore(create_info));
}

void Semaphore::destroy() {
    graphics_core_->device.destroySemaphore(**this);
}
}
