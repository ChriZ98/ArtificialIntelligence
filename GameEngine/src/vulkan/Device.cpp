﻿#include "stdafx.h"
#include "Device.h"
#include "core/DebugUtils.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

Device::Device(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Create logical device");

    auto &indices = graphics_core.physical_device.queue_family_indices;
    std::vector<vk::DeviceQueueCreateInfo> queue_create_infos;
    std::set<int> unique_queue_families = {indices.graphics_family, indices.present_family};

    float prio = 1.0f;
    for (int queue_family : unique_queue_families) {
        queue_create_infos.emplace_back(vk::DeviceQueueCreateFlags(), queue_family, 1, &prio);
    }

    vk::DeviceCreateInfo create_info;
    create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size());
    create_info.pQueueCreateInfos = queue_create_infos.data();
    create_info.enabledExtensionCount = static_cast<uint32_t>(graphics_core.device_extensions.size());
    create_info.ppEnabledExtensionNames = graphics_core.device_extensions.data();

#ifndef NDEBUG
    create_info.enabledLayerCount = static_cast<uint32_t>(graphics_core.validation_layers.size());
    create_info.ppEnabledLayerNames = graphics_core.validation_layers.data();
#endif

    debug(create_info);
    assign(graphics_core.physical_device.createDevice(create_info));

    graphics_queue = graphics_core.device.getQueue(indices.graphics_family, 0);
    present_queue = graphics_core.device.getQueue(indices.present_family, 0);
}

void Device::destroy() {
    (**this).destroy();
}

void Device::debug(const VkDeviceCreateInfo &create_info) {
    LOG_ADD(DEBUG, "queues:");
    for (size_t i = 0; i < create_info.queueCreateInfoCount; ++i) {
        LOG_ADD(DEBUG, "    queue #" << i << ":");
        DebugUtils::debug_table(DEBUG, 8,
            "flags", create_info.pQueueCreateInfos[i].flags,
            "queue count", create_info.pQueueCreateInfos[i].queueCount,
            "queue family index", create_info.pQueueCreateInfos[i].queueFamilyIndex);
    }
}
}
