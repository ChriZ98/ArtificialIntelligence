﻿#include "stdafx.h"
#include "Instance.h"
#include "core/DebugUtils.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

Instance::Instance(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Create Vulkan instance");

    vk::ApplicationInfo info;
    info.pApplicationName = graphics_core.window.title.c_str();
    info.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
    info.pEngineName = "zindach_game_engine";
    info.engineVersion = VK_MAKE_VERSION(0, 1, 0);
    info.apiVersion = VK_API_VERSION_1_0;

    std::vector<const char*> extensions = get_required_extensions();

    vk::InstanceCreateInfo create_info;
    create_info.pApplicationInfo = &info;
    create_info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    create_info.ppEnabledExtensionNames = extensions.data();

#ifndef NDEBUG
    create_info.enabledLayerCount = static_cast<uint32_t>(graphics_core.validation_layers.size());
    create_info.ppEnabledLayerNames = graphics_core.validation_layers.data();
#endif

    debug(create_info);
    assign(createInstance(create_info));
}

void Instance::destroy() {
    (**this).destroy();
}

std::vector<const char *> Instance::get_required_extensions() {
    std::vector<const char *> extensions;
    uint32_t glfw_extension_count = 0;
    const char **glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

    for (unsigned int i = 0; i < glfw_extension_count; ++i) {
        extensions.push_back(glfw_extensions[i]);
    }

#ifndef NDEBUG
    extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
#endif
    return extensions;
}

void Instance::debug(const vk::InstanceCreateInfo &create_info) {
    DebugUtils::debug_table(DEBUG, 0,
        "api version", DebugUtils::debug_version(create_info.pApplicationInfo->apiVersion),
        "application name", create_info.pApplicationInfo->pApplicationName,
        "application version", DebugUtils::debug_version(create_info.pApplicationInfo->applicationVersion),
        "engine name", create_info.pApplicationInfo->pEngineName,
        "engine version", DebugUtils::debug_version(create_info.pApplicationInfo->engineVersion),
        "flags", to_string(create_info.flags),
        "next", create_info.pApplicationInfo->pNext);

    LOG_ADD(VERBOSE, "available layers:");
    for (auto &entry : vk::enumerateInstanceLayerProperties()) {
        LOG_ADD(VERBOSE, "    impl: " << DebugUtils::debug_version(entry.implementationVersion) <<
            "\tspec: " << DebugUtils::debug_version(entry.specVersion) <<
            "\t" << entry.layerName);
    }

    LOG_ADD(DEBUG, "enabled layers:");
    DebugUtils::debug_array(create_info.ppEnabledLayerNames, create_info.enabledLayerCount);

    LOG_ADD(VERBOSE, "available extensions:");
    for (auto &entry : vk::enumerateInstanceExtensionProperties()) {
        LOG_ADD(VERBOSE, "    spec: " << DebugUtils::debug_version(entry.specVersion) <<
            "\t" << entry.extensionName);
    }

    LOG_ADD(DEBUG, "enabled extensions:");
    DebugUtils::debug_array(create_info.ppEnabledExtensionNames, create_info.enabledExtensionCount);
}
}
