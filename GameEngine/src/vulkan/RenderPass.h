﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class RenderPass : public VulkanWrapper<vk::RenderPass> {
    static void debug(const vk::RenderPassCreateInfo &create_info);

public:
    explicit RenderPass(GraphicsCore &graphics_core);
    void destroy();
};
}
