﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {
class GraphicsCore;

class Instance : public VulkanWrapper<vk::Instance> {
    static std::vector<const char *> get_required_extensions();
    static void debug(const vk::InstanceCreateInfo &create_info);

public:
    explicit Instance(GraphicsCore &graphics_core);
    void destroy();
};
}
