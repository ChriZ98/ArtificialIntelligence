﻿#include "stdafx.h"
#include "CommandPool.h"
#include "core/Logger.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

CommandPool::CommandPool(GraphicsCore &graphics_core)
    : VulkanWrapper(graphics_core) {
    LOG(INFO, "Create command pool");

    vk::CommandPoolCreateInfo create_info;
    create_info.queueFamilyIndex = graphics_core.physical_device.queue_family_indices.graphics_family;

    assign(graphics_core.device.createCommandPool(create_info));
}

void CommandPool::destroy() {
    graphics_core_->device.destroyCommandPool(**this);
}
}
