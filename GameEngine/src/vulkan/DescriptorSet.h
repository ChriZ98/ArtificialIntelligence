﻿#pragma once
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class DescriptorSet :public VulkanWrapper<vk::DescriptorSet>{
public:
    explicit DescriptorSet(GraphicsCore &graphics_core);
};
}
