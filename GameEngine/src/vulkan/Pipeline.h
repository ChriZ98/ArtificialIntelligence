﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class Pipeline : public VulkanWrapper<vk::Pipeline> {
    vk::PipelineShaderStageCreateInfo create_shader_stage(const std::string &filename,
                                                          const vk::ShaderStageFlagBits shader_type) const;
    static void debug(const vk::GraphicsPipelineCreateInfo &create_info);

public:
    vk::PipelineLayout layout;

    explicit Pipeline(GraphicsCore &graphics_core);
    void destroy();
};
}
