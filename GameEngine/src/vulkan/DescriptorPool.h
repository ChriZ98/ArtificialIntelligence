﻿#pragma once
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class DescriptorPool :public VulkanWrapper<vk::DescriptorPool>{
public:
    explicit DescriptorPool(GraphicsCore &graphics_core);
    void destroy();
};
}
