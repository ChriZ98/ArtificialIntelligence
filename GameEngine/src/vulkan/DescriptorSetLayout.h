﻿#pragma once
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class DescriptorSetLayout : public VulkanWrapper<vk::DescriptorSetLayout> {
public:
    explicit DescriptorSetLayout(GraphicsCore &graphics_core);
    void destroy();
};
}
