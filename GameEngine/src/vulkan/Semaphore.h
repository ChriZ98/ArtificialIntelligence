﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class Semaphore : public VulkanWrapper<vk::Semaphore> {
public:
    explicit Semaphore(GraphicsCore &graphics_core);
    void destroy();
};
}
