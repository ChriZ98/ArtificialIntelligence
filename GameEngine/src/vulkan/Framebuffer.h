﻿#pragma once
#include "stdafx.h"
#include "graphics/VulkanWrapper.h"

namespace zindach_game_engine {

class Framebuffer : public VulkanWrapper<vk::Framebuffer> {
public:
    Framebuffer(GraphicsCore &graphics_core, const vk::ImageView &image_view);
    void destroy();

    static std::vector<Framebuffer> create_vector(GraphicsCore &graphics_core);
    static void destroy_vector(std::vector<Framebuffer> &framebuffers);
};
}
