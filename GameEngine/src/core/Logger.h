#pragma once
#include <string>
#include <vector>

#define LOG(SEVERITY, MESSAGE) \
	if(SEVERITY >= Logger::log_level) {\
		Logger::log(SEVERITY, __FILE__) << MESSAGE; \
		if(SEVERITY == ERROR) {\
            exit(-1); \
		} \
	}

#define LOG_ADD(SEVERITY, MESSAGE) \
	if(SEVERITY >= Logger::log_level) {\
		Logger::add() << MESSAGE; \
	}

namespace zindach_game_engine {

enum LogSeverity {
    VERBOSE,
    DEBUG,
    INFO,
    WARNING,
    CRITICAL,
    ERROR
};

class Logger {
    static const std::vector<std::string> serverity_names;

public:
#ifdef NDEBUG
	static constexpr int log_level = 2;
#else
    static constexpr int log_level = 1;
#endif
    static std::ostream &log(const LogSeverity severity, const std::string &origin);
    static std::ostream &add();
};
}
