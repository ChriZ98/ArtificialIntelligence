#include "stdafx.h"
#include "Core.h"
#include "Logger.h"

namespace zindach_game_engine {

Core::Core(const unsigned int fps, GraphicsCore &graphics_core)
    : frames_per_second_(fps), graphics_core_(graphics_core) {
    LOG(INFO, "Initialized core");
    LOG_ADD(DEBUG, "FPS: " << frames_per_second_);
}

void Core::run() const {
    LOG(INFO, "Running Game loop");

    unsigned short fps = 0, ticks = 0;
    auto last_time = std::chrono::steady_clock::now();
    unsigned long long second_time = 0;
    unsigned long long passed_time_sum = 0;

    while (!graphics_core_.window.should_close) {
        const auto now = std::chrono::steady_clock::now();
        const auto passed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(now - last_time).count();
        last_time = now;
        passed_time_sum += passed_time;

        second_time += passed_time;

        //update(frame_time / 1000000000.0);
        graphics_core_.update_uniform_buffer();
        ++ticks;

        if (second_time >= 1000000000) {
            LOG(INFO, "FPS: " << fps << ", Ticks: " << ticks << ", Time: " << static_cast<double>(passed_time_sum) /
                static_cast<double>(fps) / 1000000.0f << " ms");
            second_time = 0;
            passed_time_sum = 0;
            fps = 0;
            ticks = 0;
        }

        graphics_core_.render();
        ++fps;

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    graphics_core_.clean_up();
}
}
