#include "Logger.h"
#include <iomanip>
#include <iostream>
#include <time.h>
#include <Windows.h>

namespace zindach_game_engine {

const std::vector<std::string> Logger::serverity_names{"VERB", "DBUG","INFO","WARN","CRIT","ERRO"};
const HANDLE h_std_out = GetStdHandle(STD_OUTPUT_HANDLE);

std::ostream &Logger::log(const LogSeverity severity, const std::string &origin) {
    const size_t origin_index = origin.find_last_of("\\") + 1;
    time_t t = time(nullptr);
    tm buf;

    localtime_s(&buf, &t);
    const auto time = std::put_time(&buf, "%Y-%m-%d %OH:%OM:%OS");

    SetConsoleTextAttribute(h_std_out, FOREGROUND_INTENSITY);
    std::clog << std::boolalpha << "\n[" << serverity_names[severity] << "] [" << time << "] [" << origin.substr(
        origin_index, origin.find_last_of(".") - origin_index) << "]\n";
    SetConsoleTextAttribute(h_std_out,FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    return std::clog;
}

std::ostream &Logger::add() {
    return std::clog << "\n    ";
}
}
