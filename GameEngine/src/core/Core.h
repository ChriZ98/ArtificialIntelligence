#pragma once
#include "stdafx.h"
#include "graphics/GraphicsCore.h"

namespace zindach_game_engine {

class Core {
protected:
    const unsigned int frames_per_second_;
    GraphicsCore &graphics_core_;

public:
    Core(const unsigned int fps, GraphicsCore &graphics_core);

    void run() const;
};
}
