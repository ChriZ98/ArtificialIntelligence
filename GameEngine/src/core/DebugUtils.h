﻿#pragma once
#include "stdafx.h"
#include "Logger.h"

namespace zindach_game_engine {
struct SwapChainSupportDetail;

struct DebugUtils {
    static std::string debug_offset(const vk::Offset2D &offset);
    static std::string debug_extent(const vk::Extent2D &extent);
    static std::string debug_extent(const vk::Extent3D &extent);
    static std::string debug_version(const uint32_t &version);

    static void debug_array(const char *const*array, const uint32_t size);

    template <typename S, typename T>
    static size_t debug_table(const LogSeverity severity, const size_t name_length, const size_t offset,
                              const S &name_arg, const T &value);
    template <typename ... Args, typename S, typename T>
    static size_t debug_table(const LogSeverity severity, const size_t name_length, const size_t offset,
                              const S &name_arg, const T &value, Args ... args);
    template <typename ...Args>
    static void debug_table(const LogSeverity severity, const size_t offset, Args ...args);
};

template <typename S, typename T>
size_t DebugUtils::debug_table(const LogSeverity severity, const size_t name_length, const size_t offset,
                               const S &name_arg, const T &value) {
    std::string name(name_arg);
    const size_t max_length = name.size() > name_length ? name.size() : name_length;

    LOG_ADD(severity, std::string(offset, ' ') << name << ":" << std::string(max_length + 1 - name.length(), ' ') << value);

    return max_length;
}

template <typename ... Args, typename S, typename T>
size_t DebugUtils::debug_table(const LogSeverity severity, const size_t name_length, const size_t offset,
                               const S &name_arg, const T &value, Args ...args) {
    std::string name(name_arg);
    const size_t max_length = debug_table(severity, name.size() > name_length ? name.size() : name_length, offset, args...);

    LOG_ADD(severity, std::string(offset, ' ') << name << ":" << std::string(max_length + 1 - name.length(), ' ') << value);

    return max_length;
}

template <typename ... Args>
void DebugUtils::debug_table(const LogSeverity severity, const size_t offset, Args ...args) {
    debug_table(severity, static_cast<size_t>(0), offset, args...);
}
}
