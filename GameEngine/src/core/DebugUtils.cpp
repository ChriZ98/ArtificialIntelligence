﻿#include "stdafx.h"
#include "DebugUtils.h"

namespace zindach_game_engine {

void DebugUtils::debug_array(const char * const*array, const uint32_t size) {
    for (uint32_t i = 0; i < size; ++i) {
        LOG_ADD(DEBUG, "    " << array[i]);
    }
}

std::string DebugUtils::debug_offset(const vk::Offset2D &offset) {
    return "(" + std::to_string(offset.x) + ", " + std::to_string(offset.y) + ")";
}

std::string DebugUtils::debug_extent(const vk::Extent2D &extent) {
    return "(" + std::to_string(extent.width) + ", " + std::to_string(extent.height) + ")";
}

std::string DebugUtils::debug_extent(const vk::Extent3D &extent) {
    return "(" + std::to_string(extent.width) + ", " +
           std::to_string(extent.height) + ", " +
           std::to_string(extent.depth) + ")";
}

std::string DebugUtils::debug_version(const uint32_t &version) {
    return std::to_string(VK_VERSION_MAJOR(version)) + "." + std::to_string(VK_VERSION_MINOR(version)) + "." + std::
           to_string(VK_VERSION_PATCH(version));
}
}
