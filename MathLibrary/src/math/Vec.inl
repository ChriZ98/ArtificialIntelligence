#pragma once

namespace zindach_math {

template <typename T, size_t Size>
Vec<T, Size>::Vec()
    : data{} {}

template <typename T, size_t Size>
Vec<T, Size>::Vec(const T value)
    : data{} {
    if (value != static_cast<T>(0)) {
        data.fill(value);
    }
}

template <typename T, size_t Size>
Vec<T, Size>::Vec(const std::array<T, Size> &values)
    : data(values) { }

template <typename T, size_t Size>
template <typename S>
Vec<T, Size>::Vec(const Vec<S, Size> &other)
    : data() {
    for (size_t i = 0; i < Size; ++i) {
        data[i] = static_cast<T>(other(i));
    }
}

template <typename T, size_t Size>
template <typename ... Args>
Vec<T, Size>::Vec(const Args ... values)
    : data({values...}) {}

template <typename T, size_t Size>
T &Vec<T, Size>::operator()(const size_t index) {
    return data[index];
}

template <typename T, size_t Size>
T Vec<T, Size>::operator()(const size_t index) const {
    return data[index];
}

template <typename T, size_t Size>
constexpr size_t Vec<T, Size>::size() {
    return Size;
}

template <typename T, size_t Size>
Vec<T, Size> Vec<T, Size>::operator+=(const Vec<T, Size> &rhs) {
    for (size_t i = 0; i < Size; ++i) {
        data[i] += rhs(i);
    }
    return *this;
}

template <typename T, size_t Size>
Vec<T, Size> Vec<T, Size>::operator-=(const Vec<T, Size> &rhs) {
    for (size_t i = 0; i < Size; ++i) {
        data[i] -= rhs(i);
    }
    return *this;
}

template <typename T, size_t Size>
Vec<T, Size> Vec<T, Size>::operator*=(const T rhs) {
    for (auto &value : data) {
        value *= rhs;
    }
    return *this;
}

template <typename T, size_t Size>
Vec<T, Size> Vec<T, Size>::operator/=(const T rhs) {
    for (auto &value : data) {
        value /= rhs;
    }
    return *this;
}

template <typename T, size_t Size>
T Vec<T, Size>::length() const {
    return sqrt(dot(*this));
}

template <typename T, size_t Size>
T Vec<T, Size>::dot(const Vec<T, Size> &rhs) const {
    T sum = static_cast<T>(0);

    for (size_t i = 0; i < Size; ++i) {
        sum += data[i] * rhs(i);
    }

    return sum;
}

template <typename T, size_t Size>
Vec<T, Size> Vec<T, Size>::normalize() {
    T len = length();
    if (len == static_cast<T>(0)) {
        return *this;
    }

    return *this /= len;
}

template <typename T, size_t Size>
Vec<T, Size> operator+(Vec<T, Size> lhs, const Vec<T, Size> &rhs) {
    return lhs += rhs;
}

template <typename T, size_t Size>
Vec<T, Size> operator-(Vec<T, Size> lhs, const Vec<T, Size> &rhs) {
    return lhs -= rhs;
}

template <typename T, size_t Size>
Vec<T, Size> operator*(const T lhs, Vec<T, Size> rhs) {
    return lhs *= rhs;
}

template <typename T, size_t Size>
Vec<T, Size> operator*(Vec<T, Size> lhs, const T rhs) {
    return lhs *= rhs;
}

template <typename T, size_t Size>
Vec<T, Size> operator/(Vec<T, Size> lhs, const T rhs) {
    return lhs /= rhs;
}

template <typename T, size_t Size>
bool operator==(const Vec<T, Size> &lhs, const Vec<T, Size> &rhs) {
    for (size_t i = 0; i < Size; ++i) {
        if (lhs(i) != rhs(i)) {
            return false;
        }
    }

    return true;
}

template <typename T, size_t Size>
bool operator!=(const Vec<T, Size> &lhs, const Vec<T, Size> &rhs) {
    for (size_t i = 0; i < Size; ++i) {
        if (lhs(i) != rhs(i)) {
            return true;
        }
    }

    return false;
}

template <typename T, size_t Size>
std::ostream &operator<<(std::ostream &os, const Vec<T, Size> &rhs) {
    for (size_t i = 0; i < Size; ++i) {
        os << rhs(i);
        if (i < Size - 1) {
            os << " ";
        }
    }

    return os;
}

template <typename T, size_t Size>
std::string zindach_math::to_string(const Vec<T, Size> &vec) {
    std::string result = "(";

    for (size_t i = 0; i < Size; ++i) {
        result += std::to_string(vec(i));

        if (i < Size - 1) {
            result += " ";
        }
    }

    return result + ")";
}

template <typename T, size_t Size>
Vec<T, Size> normalize(Vec<T, Size> vec) {
    return vec.normalize();
}

template <typename T>
vec3<T> cross(const vec3<T> &lhs, const vec3<T> &rhs) {
    return vec3<T>(
        lhs(1) * rhs(2) - lhs(2) * rhs(1),
        lhs(2) * rhs(0) - lhs(0) * rhs(2),
        lhs(0) * rhs(1) - lhs(1) * rhs(0)
    );
}
}
