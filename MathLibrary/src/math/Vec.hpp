﻿#pragma once
#include <array>

namespace zindach_math {

template <typename T, size_t Size>
class Vec {
public:
    std::array<T, Size> data;

    Vec();
    template <typename S>
    explicit Vec(const Vec<S, Size> &other);
    explicit Vec(const T value);
    explicit Vec(const std::array<T, Size> &values);
    template <typename ...Args>
    explicit Vec(const Args ...values);

    T &operator()(const size_t index);
    T operator()(const size_t index) const;

    static constexpr size_t size();

    Vec<T, Size> operator+=(const Vec<T, Size> &rhs);
    Vec<T, Size> operator-=(const Vec<T, Size> &rhs);
    Vec<T, Size> operator*=(const T rhs);
    Vec<T, Size> operator/=(const T rhs);

    T length() const;
    T dot(const Vec<T, Size> &rhs) const;
    Vec<T, Size> normalize();
};

template <typename T>
using vec2 = Vec<T, 2>;

using vec2_f = vec2<float>;
using vec2_ui = vec2<unsigned int>;

template <typename T>
using vec3 = Vec<T, 3>;

using vec3_f = vec3<float>;

template <typename T>
using vec4 = Vec<T, 4>;

using vec4_f = vec4<float>;

template <typename T, size_t Size>
Vec<T, Size> operator+(Vec<T, Size> lhs, const Vec<T, Size> &rhs);

template <typename T, size_t Size>
Vec<T, Size> operator-(Vec<T, Size> lhs, const Vec<T, Size> &rhs);

template <typename T, size_t Size>
Vec<T, Size> operator*(const T lhs, Vec<T, Size> rhs);

template <typename T, size_t Size>
Vec<T, Size> operator*(Vec<T, Size> lhs, const T rhs);

template <typename T, size_t Size>
Vec<T, Size> operator/(Vec<T, Size> lhs, const T rhs);

template <typename T, size_t Size>
bool operator==(const Vec<T, Size> &lhs, const Vec<T, Size> &rhs);

template <typename T, size_t Size>
bool operator!=(const Vec<T, Size> &lhs, const Vec<T, Size> &rhs);

template <typename T, size_t Size>
std::ostream &operator<<(std::ostream &os, const Vec<T, Size> &rhs);

template <typename T, size_t Size>
std::string to_string(const Vec<T, Size> &vec);

template <typename T, size_t Size>
Vec<T, Size> normalize(Vec<T, Size> vec);

template <typename T>
vec3<T> cross(const vec3<T> &lhs, const vec3<T> &rhs);
}

#include "Vec.inl"
