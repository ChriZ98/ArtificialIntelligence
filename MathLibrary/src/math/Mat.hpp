﻿#pragma once
#include <array>
#include <string>
#include "Vec.hpp"

namespace zindach_math {

template <typename T, size_t Rows, size_t Cols>
class Mat {
public:
    std::array<T, Rows * Cols> data;

    Mat();
    template <typename S>
    explicit Mat(const Mat<S, Rows, Cols> &other);
    explicit Mat(const T value);
    explicit Mat(const std::array<T, Rows * Cols> &values);
    template <typename ...Args>
    explicit Mat(const Args ...values);

    T &operator()(const size_t index);
    T operator()(const size_t index) const;
    T &operator()(const size_t i, const size_t j);
    T operator()(const size_t i, const size_t j) const;

    static constexpr size_t rows();
    static constexpr size_t cols();
    static constexpr size_t size();

    Mat<T, Rows, Cols> operator+=(const Mat<T, Rows, Cols> &rhs);
    Mat<T, Rows, Cols> operator-=(const Mat<T, Rows, Cols> &rhs);
    Mat<T, Rows, Cols> operator*=(const float rhs);
    Mat<T, Rows, Cols> operator/=(const float rhs);
};

template <typename T, size_t Size>
using mat = Mat<T, Size, Size>;

template <typename T>
using mat3 = mat<T, 3>;

using mat3_f = mat3<float>;

template <typename T>
using mat4 = mat<T, 4>;

using mat4_f = mat4<float>;

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator+(Mat<T, Rows, Cols> lhs, const Mat<T, Rows, Cols> &rhs);

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator-(Mat<T, Rows, Cols> lhs, const Mat<T, Rows, Cols> &rhs);

template <typename T, size_t Rows, size_t Cols, size_t P>
Mat<T, Rows, Cols> operator*(const Mat<T, Rows, P> &lhs, const Mat<T, P, Cols> &rhs);

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator*(const T lhs, Mat<T, Rows, Cols> rhs);

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator*(Mat<T, Rows, Cols> lhs, const T rhs);

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator/(Mat<T, Rows, Cols> lhs, const T rhs);

template <typename T, size_t Rows, size_t Cols>
bool operator==(const Mat<T, Rows, Cols> &lhs, const Mat<T, Rows, Cols> &rhs);

template <typename T, size_t Rows, size_t Cols>
bool operator!=(const Mat<T, Rows, Cols> &lhs, const Mat<T, Rows, Cols> &rhs);

template <typename T, size_t Rows, size_t Cols>
std::ostream &operator<<(std::ostream &os, const Mat<T, Rows, Cols> &rhs);

template <typename T, size_t Rows, size_t Cols>
std::string to_string(const Mat<T, Cols, Rows> &mat);

template <typename T, size_t Rows, size_t Cols>
Mat<T, Cols, Rows> transpose(const Mat<T, Rows, Cols> &mat);

template <typename T>
mat3<T> translation_matrix(const vec2<T> &translate);

template <typename T>
mat4<T> translation_matrix(const vec3<T> &translate);

template <typename T>
mat3<T> rotation_matrix(const T angle);

template <typename T>
mat4<T> rotation_matrix(const vec3<T> &axis, const T angle);

template <typename T>
mat3<T> scale_matrix(const vec2<T> &scale);

template <typename T>
mat4<T> scale_matrix(const vec3<T> &scale);

template <typename T>
mat4<T> perspective_projection(const T fov, const T near, const T far, const T width, const T height);

template <typename T>
mat4<T> view_projection(const vec3<T> &pos, const vec3<T> &target, const vec3<T> &up);
}

#include "Mat.inl"
