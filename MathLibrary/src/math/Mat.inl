#pragma once
#include "Constants.h"

namespace zindach_math {

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols>::Mat()
    : data{} {
    if (Rows == Cols) {
        for (size_t i = 0; i < Rows; ++i) {
            data[i * Cols + i] = static_cast<T>(1);
        }
    }
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols>::Mat(const T value)
    : data{} {
    if (value == static_cast<T>(0)) {
        return;
    }

    if (Rows == Cols) {
        for (size_t i = 0; i < Rows; ++i) {
            data[i * Cols + i] = value;
        }
    } else {
        data.fill(value);
    }
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols>::Mat(const std::array<T, Rows * Cols> &values)
    : data(values) { }

template <typename T, size_t Rows, size_t Cols>
template <typename S>
Mat<T, Rows, Cols>::Mat(const Mat<S, Rows, Cols> &other)
    : data() {
    for (size_t i = 0; i < Rows * Cols; ++i) {
        data[i] = static_cast<T>(other(i));
    }
}

template <typename T, size_t Rows, size_t Cols>
template <typename ...Args>
Mat<T, Rows, Cols>::Mat(const Args ...values)
    : data({values...}) { }

template <typename T, size_t Rows, size_t Cols>
T &Mat<T, Rows, Cols>::operator()(const size_t index) {
    return data[index];
}

template <typename T, size_t Rows, size_t Cols>
T Mat<T, Rows, Cols>::operator()(const size_t index) const {
    return data[index];
}

template <typename T, size_t Rows, size_t Cols>
T &Mat<T, Rows, Cols>::operator()(const size_t i, const size_t j) {
    return data[i * Cols + j];
}

template <typename T, size_t Rows, size_t Cols>
T Mat<T, Rows, Cols>::operator()(const size_t i, const size_t j) const {
    return data[i * Cols + j];
}

template <typename T, size_t Rows, size_t Cols>
constexpr size_t Mat<T, Rows, Cols>::rows() {
    return Rows;
}

template <typename T, size_t Rows, size_t Cols>
constexpr size_t Mat<T, Rows, Cols>::cols() {
    return Cols;
}

template <typename T, size_t Rows, size_t Cols>
constexpr size_t Mat<T, Rows, Cols>::size() {
    return Rows * Cols;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> Mat<T, Rows, Cols>::operator+=(const Mat<T, Rows, Cols> &rhs) {
    for (size_t i = 0; i < Rows * Cols; ++i) {
        data[i] += rhs(i);
    }
    return *this;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> Mat<T, Rows, Cols>::operator-=(const Mat<T, Rows, Cols> &rhs) {
    for (size_t i = 0; i < Rows * Cols; ++i) {
        data[i] -= rhs(i);
    }
    return *this;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> Mat<T, Rows, Cols>::operator*=(const float rhs) {
    for (auto &value : data) {
        value *= rhs;
    }
    return *this;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> Mat<T, Rows, Cols>::operator/=(const float rhs) {
    for (auto &value : data) {
        value /= rhs;
    }
    return *this;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator+(Mat<T, Rows, Cols> lhs, const Mat<T, Rows, Cols> &rhs) {
    return lhs += rhs;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator-(Mat<T, Rows, Cols> lhs, const Mat<T, Rows, Cols> &rhs) {
    return lhs -= rhs;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator*(const T lhs, Mat<T, Rows, Cols> rhs) {
    return rhs *= lhs;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator*(Mat<T, Rows, Cols> lhs, const T rhs) {
    return lhs *= rhs;
}

template <typename T, size_t Rows, size_t Cols, size_t P>
Mat<T, Rows, Cols> operator*(const Mat<T, Rows, P> &lhs, const Mat<T, P, Cols> &rhs) {
    Mat<T, Rows, Cols> result(static_cast<T>(0));

    for (size_t i = 0; i < Rows; ++i) {
        for (size_t k = 0; k < P; ++k) {
            for (size_t j = 0; j < Cols; ++j) {
                result(i, j) += lhs(i, k) * rhs(k, j);
            }
        }
    }

    return result;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Rows, Cols> operator/(Mat<T, Rows, Cols> lhs, const T rhs) {
    return lhs /= rhs;
}

template <typename T, size_t Rows, size_t Cols>
bool operator==(const Mat<T, Rows, Cols> &lhs, const Mat<T, Rows, Cols> &rhs) {
    for (size_t i = 0; i < Rows * Cols; ++i) {
        if (lhs(i) != rhs(i)) {
            return false;
        }
    }

    return true;
}

template <typename T, size_t Rows, size_t Cols>
bool operator!=(const Mat<T, Rows, Cols> &lhs, const Mat<T, Rows, Cols> &rhs) {
    for (size_t i = 0; i < Rows * Cols; ++i) {
        if (lhs(i) != rhs(i)) {
            return true;
        }
    }

    return false;
}

template <typename T, size_t Rows, size_t Cols>
std::ostream &operator<<(std::ostream &os, const Mat<T, Rows, Cols> &rhs) {
    for (size_t i = 0; i < Rows; ++i) {
        for (int j = 0; j < Cols; ++j) {
            os << rhs(i, j);
            if (j < Cols - 1) {
                os << " ";
            }
        }

        if (i < Rows - 1) {
            os << "\n";
        }
    }

    return os;
}

template <typename T, size_t Rows, size_t Cols>
std::string to_string(const Mat<T, Cols, Rows> &mat) {
    std::string result = "";

    for (size_t i = 0; i < Rows; ++i) {
        for (int j = 0; j < Cols; ++j) {
            result += std::to_string(mat(i, j));
            if (j < Cols - 1) {
                result += " ";
            }
        }

        if (i < Rows - 1) {
            result += "\n";
        }
    }

    return result;
}

template <typename T, size_t Rows, size_t Cols>
Mat<T, Cols, Rows> transpose(const Mat<T, Rows, Cols> &mat) {
    Mat<T, Cols, Rows> result(static_cast<T>(0));

    for (size_t i = 0; i < Rows; ++i) {
        for (size_t j = 0; j < Cols; ++j) {
            result(j, i) = mat(i, j);
        }
    }

    return result;
}

template <typename T>
mat3<T> translation_matrix(const vec2<T> &translate) {
    mat3<T> result;
    result(0, 2) = translate(0);
    result(1, 2) = translate(1);
    return result;
}

template <typename T>
mat4<T> translation_matrix(const vec3<T> &translate) {
    mat4<T> result;
    result(0, 2) = translate(0);
    result(1, 2) = translate(1);
    result(2, 2) = translate(2);
    return result;
}

template <typename T>
mat3<T> rotation_matrix(const T angle) {
    mat3<T> result;
    result(0, 0) = cos(angle);
    result(0, 1) = sin(angle);
    result(1, 0) = -sin(angle);
    result(1, 1) = cos(angle);
    return result;
}

template <typename T>
mat4<T> rotation_matrix(const vec3<T> &axis, const T angle) {
    const T co = cos(angle);
    const T co1 = 1 - co;
    const T si = sin(angle);

    mat4<T> result;
    result(0, 0) = co + axis(0) * axis(0) * co1;
    result(0, 1) = axis(0) * axis(1) * co1 - axis(2) * si;
    result(0, 2) = axis(0) * axis(2) * co1 + axis(1) * si;
    result(1, 0) = axis(0) * axis(1) * co1 + axis(2) * si;
    result(1, 1) = co + axis(1) * axis(1) * co1;
    result(1, 2) = axis(1) * axis(2) * co1 - axis(0) * si;
    result(2, 0) = axis(0) * axis(2) * co1 - axis(1) * si;
    result(2, 1) = axis(1) * axis(2) * co1 + axis(0) * si;
    result(2, 2) = co + axis(2) * axis(2) * co1;
    return result;
}

template <typename T>
mat3<T> scale_matrix(const vec2<T> &scale) {
    mat3<T> result;
    result(0, 0) = scale(0);
    result(1, 1) = scale(1);
    return result;
}

template <typename T>
mat4<T> scale_matrix(const vec3<T> &scale) {
    mat4<T> result;
    result(0, 0) = scale(0);
    result(1, 1) = scale(1);
    result(2, 2) = scale(2);
    return result;
}

template <typename T>
mat4<T> perspective_projection(const T fov, const T near, const T far, const T width, const T height) {
    const float s = cos(fov * pi / 360.0f) / sin(fov * pi / 360.0f);

    mat4<T> result(0.0f);
    result(0, 0) = s / width * height;
    result(1, 1) = -s;
    result(2, 2) = -(far + near) / (far - near);
    result(2, 3) = -(2 * near * far) / (far - near);
    result(3, 2) = -1;
    return result;
}

template <typename T>
mat4<T> view_projection(const vec3<T> &pos, const vec3<T> &target, const vec3<T> &up) {
    vec3<T> t = normalize(target - pos);
    vec3<T> r = cross(t, up).normalize();
    vec3<T> u = cross(r, t);

    mat4<T> result;
    result(0, 0) = r(0);
    result(0, 1) = r(1);
    result(0, 2) = r(2);
    result(0, 3) = -r.dot(pos);
    result(1, 0) = u(0);
    result(1, 1) = u(1);
    result(1, 2) = u(2);
    result(1, 3) = -u.dot(pos);
    result(2, 0) = -t(0);
    result(2, 1) = -t(1);
    result(2, 2) = -t(2);
    result(2, 3) = t.dot(pos);
    return result;
}
}
