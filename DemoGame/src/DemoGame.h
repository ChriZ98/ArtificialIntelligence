﻿#pragma once

namespace zindach_demo {

class DemoGame {
public:
    explicit DemoGame();

    static void update(double frame_time);
    static void render();
};
}
