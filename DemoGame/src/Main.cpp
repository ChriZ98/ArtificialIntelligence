#include "core/Core.h"

int main() {
    zindach_game_engine::Window window("Demo Game", false, vec2_ui(800u, 600u), false, vec2_ui(800u, 400u), true);
    zindach_game_engine::GraphicsCore graphics_core(window);
    zindach_game_engine::Core core(3000, graphics_core);

    core.run();
    return 0;
}
